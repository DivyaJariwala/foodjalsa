import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack} from '../../Config/Navigator';
import OTPTextInput from 'react-native-otp-textinput';
import {width, height, totalSize} from 'react-native-dimension';
import Header from '../../Components/CustomeHeader/CustomeHeader';
import Profile from 'Images/forget.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import * as auth from 'Services/auth.js';
import AsyncStorage from '@react-native-community/async-storage';

export default class VerifyCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      seconds: 60,
      timer: null,
      enable: false,
      email: '',
      spinner: false,
    };
  }
  async componentDidMount() {
    this.setState({
      email: await AsyncStorage.getItem('@Emailaddress'),
    });
  }

  codeValidation = () => {
    if (this.state.code === '') {
      alert('Please enter your verification code');
    } else {
      this.makeRequest();
    }
  };
  resend = () => {
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });
    const data = new FormData();
    data.append('email', this.state.email);

    auth
      .forgetpassword(data)
      .then((response) => {
        console.log('---!Response!---', response);
        if (response.success === 1) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert(response.message);
          }, 1000);
        } else if (response.success === 0) {
          this.setState({
            spinner: false,
          });
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
    // this.setState({
    //   enable: true,
    //   seconds: this.state.seconds - 1
    // })
  };
  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });
    const data = new FormData();
    data.append('email', this.state.email);
    data.append('code', this.state.code);
    auth
      .checkout(data)
      .then(async (response) => {
        console.log('---!Response!---', response);

        if (response.success === 1) {
          await AsyncStorage.setItem('@Emailaddress', this.state.email);
          this.setState({
            spinner: false,
          });
          const router = authStack.reset;
          navigation.navigate(router);
          // navigation.navigate('ChangePassword');
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
  };
  render() {
    const {navigation} = this.props;

    return (
      <KeyboardAwareScrollView
        style={Styles.container}
        contentContainerStyle={{
          // flex: 1,
          flexGrow: 1,

          backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
            paddingHorizontal: 20,
          }}>
          <View
            style={{
              height: '5%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Header
              name="Verification"
              backhandler={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              paddingVertical: 50,

              // justifyContent: 'center',
              height: '95%',
            }}>
            <View style={{alignItems: 'center'}}>
              <Image source={Profile} resizeMode="contain" />
            </View>
            <View
              style={{
                marginVertical: 40,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 20,
              }}>
              <Text
                style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
                Enter the verification code we just sent you on your email
                address.
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <OTPTextInput
                ref={(e) => (this.otpInput = e)}
                containerStyle={{width: '80%'}}
                offTintColor="#000000"
                tintColor="#000000"
                textInputStyle={{borderBottomWidth:1}}
                defaultValue={this.state.code}
                handleTextChange={(text) =>
                  this.setState({
                    code: text,
                  })
                }
              />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 14,
                }}>
                <Text
                  style={[
                    styles.subTitle,
                    {
                      fontSize: 15,
                      textAlign: 'center',
                      color: '#F5B4B2',
                      marginLeft: 3,
                    },
                  ]}>
                  If you didn’t receive a code!
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.resend();
                  }}>
                  <Text
                    style={[
                      styles.subTitle,
                      {
                        fontSize: 15,
                        textAlign: 'center',
                        color: '#EC893E',
                        marginLeft: 3,
                        fontWeight: '900',
                      },
                    ]}>
                    Resend
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginVertical: 10}}>
              <CButton
                btnText="Verify"
                onPress={() => {
                  console.log('Reset');
                  this.codeValidation();
                }}
                btnStyle={{
                  marginTop: 50,
                  backgroundColor: Colors.Button_Color,
                  borderRadius: 25,
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    // flex: 1,
    display: 'flex',
    // flexDirection: 'column',
    // backgroundColor: Colors.white,
    // paddingHorizontal: 25,
    // paddingVertical: 30
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: Colors.Button_Color,
  },
});
