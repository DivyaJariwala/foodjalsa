import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import DropDown from '../../Components/DropDown';
import * as Category from '../../Services/Category';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import * as auth from 'Services/auth.js';
import Divider from '../../Components/Divider/Divider';

export default class Signup2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outletname: '',
      outletype: '',
      outdisplayname: '',
      state: '',
      city: '',
      firstname: '',
      lastname: '',
      emailaddress: '',
      mobilenumber: '',
      password: '',
      conformPassword: '',
      category: [],
      userid: '',
      token: '',
      spinner: false,
    };
  }
  componentDidMount() {
    this.setState({
      spinner: true,
    });
    this.getDetails();
    Category.category()
      .then(async (response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.data);
        if (response.success === 1) {
          this.setState({
            category: response.data,
          });

          this.setState({
            spinner: false,
          });
          // const router = mainStack.sidebar;
          // navigation.navigate(router);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  }
  getDetails = async () => {
    this.setState({
      firstname: await AsyncStorage.getItem('@firstname'),
      lastname: await AsyncStorage.getItem('@lastname'),
      emailaddress: await AsyncStorage.getItem('@email'),
      mobilenumber: await AsyncStorage.getItem('@mobilenumber'),
      password: await AsyncStorage.getItem('@password'),
      conformPassword: await AsyncStorage.getItem('@confirmpassword'),
    });
  };
  Register = () => {
    const {navigation} = this.props;
    if (this.state.outletname === '') {
      alert('Please enter your Outletname');
    } else if (this.state.outletname.length < 3) {
      alert('Outlet should conatine at least 3 char.');
    } else if (this.state.outletype === '') {
      alert('Please select your Outletype');
    } else if (this.state.state === '') {
      alert('Please enter your state');
    } else if (this.state.city === '') {
      alert('Please enter your city');
    } else {
      this.makeRequest();
    }
  };

  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({spinner: true});
    const data = new FormData();
    data.append('first_name', this.state.firstname);
    data.append('email', this.state.emailaddress);
    data.append('password', this.state.password);
    data.append('last_name', this.state.lastname);
    data.append('mobile_no', this.state.mobilenumber);
    data.append('outlet_name', this.state.outletname);
    data.append('outlet_type', this.state.outletype);
    data.append('state', this.state.state);
    data.append('city', this.state.city);

    auth
      .register(data)
      .then(async (response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.success);
        if (response.success === 1) {
          this.setState({
            userid: response.data.id,
            token: response.data.token,
          });
          await AsyncStorage.setItem(
            '@Userid',
            JSON.stringify(this.state.userid),
          );
          await AsyncStorage.setItem('@Token', this.state.token);
          await AsyncStorage.setItem('@Qrstring', response.data.qr_string);
          await AsyncStorage.setItem('@Qrcode', response.data.qr_code);
          await AsyncStorage.setItem(
            '@outletid',
            JSON.stringify(response.data.outlet_id),
          );
          this.setState({
            spinner: false,
          });
          const router = mainStack.home;
          navigation.navigate(router);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops! The email has already been taken.');
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };

  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView
        // behavior="position"
        enableOnAndroid
        style={Styles.container}
        contentContainerStyle={{
          flexGrow: 1,

          // backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            // flex: 1,
            // alignItems: 'center',
            paddingVertical: 50,
            paddingHorizontal: 20,
            // paddingHorizontal: 40,
          }}>
          <Text style={styles.subTitle}>Create Account</Text>

          <Text style={styles.H1}>Outlet Information</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CInput
            placeholder={'Outlet Name'}
            value={this.state.outletname}
            onChangeText={(text) =>
              this.setState({
                outletname: text,
              })
            }
          />

          <DropDown
            data={this.state.category}
            placeHolder={
              this.state.outletype === '' ? 'Select Outlet Type' : ''
            }
            labelText={this.state.outdisplayname}
            selectedValue={this.state.outdisplayname}
            onChange={(value) => {
              console.log('value ]dropdown', value);
              this.setState({
                outletype: value.id,
                outdisplayname: value.label,
              });
              console.log(this.state.outletype);
            }}
          />
          <CInput
            placeholder={'State'}
            value={this.state.state}
            onChangeText={(text) =>
              this.setState({
                state: text,
              })
            }
          />
          <CInput
            placeholder={'City'}
            value={this.state.city}
            onChangeText={(text) =>
              this.setState({
                city: text,
              })
            }
          />
        </View>

        <CButton
          btnText="Sign Up"
          onPress={() => {
            console.log('Reset');
            this.Register();
          }}
          btnStyle={{
            marginTop: 50,
            backgroundColor: Colors.Button_Color,
            borderRadius: 25,
            width: '60%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />

        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
            paddingBottom: 15,
            //  / marginBottom:10
          }}>
          <Divider Width="100%" />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text
              style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
              Already have an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                const router = authStack.login;
                navigation.navigate(router);
              }}>
              <Text
                style={[
                  styles.subTitle,
                  {
                    fontWeight: 'bold',
                    fontSize: 15,
                    textAlign: 'center',
                    color: '#EC893E',
                    marginLeft: 3,
                  },
                ]}>
                LOG IN
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    display: 'flex',
  },
});
