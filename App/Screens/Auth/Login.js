/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, StyleSheet, SafeAreaView, Image, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {authStack, mainStack} from '../../Config/Navigator';
import Background from '../../../Assets/Images/FOOD.png';
import * as auth from 'Services/auth.js';
import * as reg from '../../Constant/Perfix';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Divider from '../../Components/Divider/Divider';
import {connect} from 'react-redux';
import Action from '../../Redux/action';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',

      spinner: false,
      userid: '',
      token: '',
      userType: '',
      profile: '',
      firstname: '',
      lastname: '',
      outletid: '',
    };
  }
  loginValidation = () => {
    if (reg.Emailpattern.test(this.state.email) === false) {
      alert('Please enter your email-address');
    } else if (this.state.password === '') {
      alert('Please enter your password');
    } else {
      this.makeRequest();
    }
  };

  makeRequest = () => {
    const {navigation, usertoken} = this.props;
    this.setState({spinner: true});
    const data = new FormData();
    data.append('email', this.state.email);
    data.append('password', this.state.password);

    auth
      .login(data)
      .then(async (response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.success);
        if (response.success === 1) {
          this.setState({
            userid: response.data.id,
            token: response.data.token,
          });

          await AsyncStorage.setItem(
            '@Userid',
            JSON.stringify(this.state.userid),
          );
          console.log('User id', this.state.userid);
          await AsyncStorage.setItem('@Token', this.state.token);
          await AsyncStorage.setItem('@Qrstring', response.data.qr_string);
          await AsyncStorage.setItem('@Qrcode', response.data.qr_code);
          await AsyncStorage.setItem(
            '@outletid',
            JSON.stringify(response.data.outlet_id),
          );
          usertoken(response.data.token);
          this.setState({
            spinner: false,
          });
          const router = mainStack.home;
          navigation.navigate(router);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };
  render() {
    const {navigation, usertoken} = this.props;
    return (
      <KeyboardAwareScrollView
        style={Styles.container}
        contentContainerStyle={{
          // flex: 1,
          flexGrow: 1,

          backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            // flex: 1,
            alignItems: 'center',
            paddingVertical: 50,
            paddingHorizontal: 20,
            // paddingHorizontal: 40,
          }}>
          <Image
            source={Background}
            resizeMode="contain"
            style={{height: 70, width: 150}}></Image>
          <Text style={[styles.Heading, {marginTop: 15, fontWeight: 'bold'}]}>
            Welcome back!
          </Text>
          <Text style={styles.subTitle}>Please login to your account</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CInput
            placeholder={'Enter email'}
            value={this.state.email}
            onChangeText={(text) =>
              this.setState({
                email: text,
              })
            }
            inputStyle={Styles.textinput}
            wrapperStyle={{width: '75%'}}
          />
          <CInput
            placeholder={'Enter password'}
            value={this.state.password}
            onChangeText={(text) =>
              this.setState({
                password: text,
              })
            }
            SecureTextEntry={true}
            wrapperStyle={{width: '75%'}}
          />

          <CButton
            btnText="Login"
            onPress={() => {
              console.log('Reset');
              this.loginValidation();
            }}
            // disable
            btnStyle={{
              marginTop: 50,
              backgroundColor: Colors.Button_Color,
              borderRadius: 25,
              width: '60%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          />
          <TouchableOpacity
            onPress={() => {
              const router = authStack.forgot;
              navigation.navigate(router);
            }}>
            <Text
              style={[styles.subTitle, {fontSize: 15, textAlign: 'center'}]}>
              Forget Your Password?
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
            paddingBottom: 15,
            //  / marginBottom:10
          }}>
          <Divider Width="100%" />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text
              style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
              Don't have an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                const router = authStack.register;
                navigation.navigate(router);
              }}>
              <Text
                style={[
                  styles.subTitle,
                  {
                    fontSize: 16,
                    textAlign: 'justify',
                    color: '#EC893E',
                    marginLeft: 3,
                    fontWeight: 'bold',
                  },
                ]}>
                SIGN UP
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    // flex: 1,
  },
  textinput: {
    width: '10%',
  },
});

const mapStateToProps = (state) => {
  if (state === undefined) return {};
  console.log('State==>>', state);

  return {
    index: state.index,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    usertoken: (token) => dispatch(Action.usertoken(token)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
