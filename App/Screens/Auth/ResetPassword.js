/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack} from '../../Config/Navigator';
import Header from '../../Components/CustomeHeader/CustomeHeader';
import Profile from 'Images/reset.png';
import * as reg from '../../Constant/Perfix';
import * as auth from 'Services/auth.js';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmpassword: '',
      spinner: false,
      email: '',
    };
  }
  async componentDidMount() {
    this.setState({
      email: await AsyncStorage.getItem('@Emailaddress'),
    });
  }

  validation = () => {
    if (this.state.password === '') {
      alert('Invalid Password');
    } else if (this.state.password !== this.state.confirmpassword) {
      alert('Password does not match');
    } else {
      this.makeRequest();
    }
  };
  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });

    const data = new FormData();
    data.append('new_password', this.state.password);
    data.append('confirm_password', this.state.confirmpassword);
    data.append('email', this.state.email);
    auth
      .changepassowrd(data)
      .then((response) => {
        console.log('---!Response!---', response);
        if (response.success === 1) {
          this.setState({
            spinner: false,
          });
          const router = authStack.login;
          navigation.navigate(router);
        } else if (response.success === 0) {
          // this.setState({
          //   spinner: false
          // })
          // alert(response.error);
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.error);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
  };
  render() {
    const {navigation} = this.props;

    return (
      <KeyboardAwareScrollView
        style={Styles.container}
        contentContainerStyle={{
          // flex: 1,
          flexGrow: 1,

          backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
            paddingHorizontal: 20,
          }}>
          <View
            style={{
              height: '5%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Header
              name="Reset Password"
              backhandler={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              paddingVertical: 50,

              // justifyContent: 'center',
              height: '95%',
            }}>
            <View style={{alignItems: 'center'}}>
              <Image source={Profile} resizeMode="contain" />
            </View>
            <View
              style={{
                marginVertical: 50,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 22,
              }}>
              <Text
                style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
                Just type it twice and try to not forget it this time.
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <CInput
                placeholder={'Password'}
                value={this.state.password}
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                  })
                }
                SecureTextEntry={true}
              />
              <CInput
                placeholder={'Confirm Password'}
                value={this.state.confirmpassword}
                onChangeText={(text) =>
                  this.setState({
                    confirmpassword: text,
                  })
                }
                SecureTextEntry={true}
              />
            </View>
            <View style={{marginVertical: 10}}>
              <CButton
                btnText="Done"
                onPress={() => {
                  console.log('Reset');
                  this.validation();
                }}
                btnStyle={{
                  marginTop: 50,
                  backgroundColor: Colors.Button_Color,
                  borderRadius: 25,
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    // flexDirection: 'column',
    backgroundColor: Colors.white,
    // paddingHorizontal: 25,
    // paddingVertical: 30
  },
});
