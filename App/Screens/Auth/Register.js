/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import * as reg from '../../Constant/Perfix';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Divider from '../../Components/Divider/Divider';

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      emailaddress: '',
      mobilenumber: '',
      password: '',
      conformPassword: '',
    };
  }
  Register = () => {
    const {navigation} = this.props;
    if (this.state.firstname === '') {
      alert('Please enter your firstname');
    } else if (this.state.firstname.length < 3) {
      alert('Firstname should contain at least 3 char.');
    } else if (this.state.lastname === '') {
      alert('Please enter your lastname');
    } else if (this.state.lastname.length < 3) {
      alert('Lastname should contain at least 3 char.');
    } else if (reg.Emailpattern.test(this.state.emailaddress) === false) {
      alert('Please enter your email-address');
    } else if (this.state.mobilenumber === '') {
      alert('Please enter your mobilenumber');
    } else if (this.state.mobilenumber.length < 10) {
      alert('The mobile no must be at least 10 characters');
    } else if (this.state.password === '') {
      alert('Please enter your password');
    } else if (this.state.password.length < 8) {
      alert('The password must be at least 8 characters.');
    } else if (this.state.conformPassword !== this.state.password) {
      alert('Your password dose not match');
    } else {
      this.storeDetails();
      const router = authStack.signup2;
      navigation.navigate(router);
    }
  };
  storeDetails = async () => {
    await AsyncStorage.setItem('@firstname', this.state.firstname);
    await AsyncStorage.setItem('@lastname', this.state.lastname);
    await AsyncStorage.setItem('@email', this.state.emailaddress);
    await AsyncStorage.setItem('@mobilenumber', this.state.mobilenumber);
    await AsyncStorage.setItem('@password', this.state.password);
    await AsyncStorage.setItem('@confirmpassword', this.state.conformPassword);
  };
  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView
        // behavior="position"
        enableOnAndroid
        style={Styles.container}
        contentContainerStyle={{
          flexGrow: 1,

          // backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            // flex: 1,
            // alignItems: 'center',
            paddingVertical: 50,
            paddingHorizontal: 20,
            // paddingHorizontal: 40,
          }}>
          <Text style={styles.subTitle}>Create Account</Text>

          <Text style={styles.H1}>Personal Information</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CInput
            placeholder={'First Name'}
            value={this.state.firstname}
            onChangeText={(text) =>
              this.setState({
                firstname: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
          />
          <CInput
            placeholder={'Last Name'}
            value={this.state.lastname}
            onChangeText={(text) =>
              this.setState({
                lastname: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
          />
          <CInput
            placeholder={'Email Address'}
            value={this.state.emailaddress}
            onChangeText={(text) =>
              this.setState({
                emailaddress: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
          />
          <CInput
            placeholder={'Mobile Number'}
            value={this.state.mobilenumber}
            onChangeText={(text) =>
              this.setState({
                mobilenumber: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
            KeyBoardType="number-pad"
          />
          <CInput
            placeholder={'Password'}
            value={this.state.password}
            onChangeText={(text) =>
              this.setState({
                password: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
            SecureTextEntry={true}
          />
          <CInput
            placeholder={'Retype Password'}
            value={this.state.conformPassword}
            onChangeText={(text) =>
              this.setState({
                conformPassword: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
            SecureTextEntry={true}
          />
        </View>

        <CButton
          btnText="Next"
          onPress={() => {
            console.log('Reset');
            this.Register();
          }}
          btnStyle={{
            marginTop: 50,
            backgroundColor: Colors.Button_Color,
            borderRadius: 25,
            width: '60%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />

        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
            paddingBottom: 15,
            //  / marginBottom:10
          }}>
          <Divider Width="100%" />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text
              style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
              Already have an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                const router = authStack.login;
                navigation.navigate(router);
              }}>
              <Text
                style={[
                  styles.subTitle,
                  {
                    fontWeight: 'bold',
                    fontSize: 15,
                    textAlign: 'center',
                    color: '#EC893E',
                    marginLeft: 3,
                  },
                ]}>
                LOG IN
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: Colors.white,
    display: 'flex',
  },
});
