/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack} from '../../Config/Navigator';
import Header from '../../Components/CustomeHeader/CustomeHeader';
import Profile from 'Images/icon.png';
import * as reg from '../../Constant/Perfix';
import * as auth from 'Services/auth.js';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      spinner: false,
    };
  }
  validation = () => {
    const {navigation} = this.props;

    if (reg.Emailpattern.test(this.state.email) === false) {
      alert('Please enter your email-address');
    } else {
      this.makeRequest();
      // const router = authStack.verify;
      // navigation.navigate(router);
    }
  };
  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });
    const data = new FormData();
    data.append('email', this.state.email);

    auth
      .forgetpassword(data)
      .then(async (response) => {
        console.log('---!Response!---', response);
        if (response.success === 1) {
          await AsyncStorage.setItem('@Emailaddress', this.state.email);
          this.setState({spinner: false});
          const router = authStack.verify;
          navigation.navigate(router);
        } else if (response.success === 0) {
          // this.setState({
          //   spinner: false
          // })
          // alert(response.error)
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
  };
  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView
        style={Styles.container}
        contentContainerStyle={{
          // flex: 1,
          flexGrow: 1,

          backgroundColor: Colors.white,
        }}
        scrollIndicatorInsets="f">
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
            paddingHorizontal: 20,
          }}>
          <View
            style={{
              height: '5%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Header
              name="Forgot Password?"
              backhandler={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              paddingVertical: 50,

              // justifyContent: 'center',
              height: '95%',
            }}>
            <View style={{alignItems: 'center'}}>
              <Image source={Profile} resizeMode="contain" />
            </View>
            <View
              style={{
                marginVertical: 50,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 22,
              }}>
              <Text
                style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
                Enter the email address associated with your account.
              </Text>
              <Text
                style={[styles.extraText, {fontSize: 14, textAlign: 'center'}]}>
                We will email you a link to reset your password.
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <CInput
                placeholder={'Enter email'}
                value={this.state.email}
                onChangeText={(text) =>
                  this.setState({
                    email: text,
                  })
                }
              />
            </View>
            <View style={{marginVertical: 10}}>
              {/* <CButton
                  btnText="Send"
                  onPress={() => {
                    console.log('Reset');
                    this.makeRequest()
                  }}
                /> */}
              <CButton
                btnText="Send"
                onPress={() => {
                  console.log('Reset');
                  this.validation();
                }}
                btnStyle={{
                  marginTop: 50,
                  backgroundColor: Colors.Button_Color,
                  borderRadius: 25,
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    display: 'flex',
  },
});

export default ForgetPassword;
