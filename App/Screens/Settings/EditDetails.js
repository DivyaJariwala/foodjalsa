/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import * as reg from '../../Constant/Perfix';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Profile from '../../Services/updatedetails';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Header from '../../Components/CustomeHeader/CustomeHeader';

export default class EditDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      mobilenumber: '',
      token: '',
      spinner: false,
    };
  }
  async componentDidMount() {
    const {navigation} = this.props;
    this.setState({spinner: true, token: await AsyncStorage.getItem('@Token')});

    Profile.getpersonaldetails(this.state.token)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.success);
        if (response.success === 1) {
          this.setState({
            firstname: response.data.first_name,
            lastname: response.data.last_name,
            mobilenumber: response.data.mobile_no,
          });

          this.setState({
            spinner: false,
          });
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  }
  editDetails = () => {
    if (this.state.mobilenumber.length < 10) {
      alert('Please check your mobile number');
    } else {
      const data = new FormData();
      data.append('first_name', this.state.firstname);
      data.append('last_name', this.state.lastname);
      data.append('mobile_no', this.state.mobilenumber);

      Profile.updatepersonaldetails(this.state.token, data)
        .then((response) => {
          console.log('---!Response!---', response);
          console.log('suvess', response.success);
          if (response.success === 1) {
            this.setState({
              spinner: false,
            });
            setTimeout(() => {
              alert(response.message);
            }, 1000);
          } else if (response.success === 0) {
            this.setState({spinner: false});
            setTimeout(() => {
              Alert.alert('Oops!', response.message);
            }, 1000);
          }
        })
        .catch((error) => {
          console.log('---!ERROR!---', error);
        });
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView
        // behavior="position"
        enableOnAndroid
        style={Styles.container}
        contentContainerStyle={{
          flexGrow: 1,

          // backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            // flex: 1,
            // alignItems: 'center',
            paddingVertical: 30,
            paddingHorizontal: 20,
            flexDirection: 'row',
            width: '100%',

            // paddingHorizontal: 40,
          }}>
          <View
            style={{
              height: '5%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Header
              name="Personal Details"
              backhandler={() => navigation.goBack()}
            />
          </View>
        </View>

        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CInput
            placeholder={'First Name'}
            value={this.state.firstname}
            onChangeText={(text) =>
              this.setState({
                firstname: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
          />
          <CInput
            placeholder={'Last Name'}
            value={this.state.lastname}
            onChangeText={(text) =>
              this.setState({
                lastname: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
          />

          <CInput
            placeholder={'Mobile Number'}
            value={this.state.mobilenumber}
            onChangeText={(text) =>
              this.setState({
                mobilenumber: text,
              })
            }
            wrapperStyle={{marginTop: 8}}
            KeyBoardType="number-pad"
          />
        </View>

        <CButton
          btnText="Save"
          onPress={() => {
            console.log('Reset');
            this.editDetails();
          }}
          btnStyle={{
            marginTop: 50,
            backgroundColor: Colors.Button_Color,
            borderRadius: 25,
            width: '60%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: Colors.white,
    display: 'flex',
  },
  arrowStyle: {
    borderColor: 'black',
    borderWidth: 1,
    width: '8%',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
