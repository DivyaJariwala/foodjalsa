import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import DropDown from '../../Components/DropDown';
import * as Category from '../../Services/Category';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import * as Profile from '../../Services/updatedetails';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Header from '../../Components/CustomeHeader/CustomeHeader';

export default class EditOutlet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outletname: '',
      outletype: '',
      outletid: '',
      state: '',
      city: '',
      category: [],
      userid: '',
      token: '',
      spinner: false,
    };
  }
  async componentDidMount() {

    this.setState({spinner: true, token: await AsyncStorage.getItem('@Token')});

    Profile.getoutlet(this.state.token)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.success);
        if (response.success === 1) {
          this.setState({
            outletname: response.data.outlet_name,
            outletype: response.data.outlet_types_title,
            state: response.data.state,
            city: response.data.city,
          });
          this.getCategory();
          this.setState({
            spinner: false,
          });
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  }
  getCategory = () => {
    Category.category()
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('succeess', response.data);
        if (response.success === 1) {
          this.setState({
            category: response.data,
          });

          this.setState({
            spinner: false,
          });
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };
  updateOutlet = () => {
    this.setState({
      spinner: true,
    });
    const data = new FormData();
    data.append('outlet_name', this.state.outletname);
    data.append('outlet_type', this.state.outletid);
    data.append('state', this.state.state);
    data.append('city', this.state.city);

    Profile.updateoutlet(this.state.token, data)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('succeess', response.success);
        if (response.success === 1) {
          this.setState({
            spinner: false,
          });
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };
  render() {
    const {navigation} = this.props;

    return (
      <KeyboardAwareScrollView
        // behavior="position"
        enableOnAndroid
        style={Styles.container}
        contentContainerStyle={{
          flexGrow: 1,

          // backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            // flex: 1,
            // alignItems: 'center',
            paddingVertical: 30,
            paddingHorizontal: 20,
            flexDirection: 'row',
            width: '100%',

            // paddingHorizontal: 40,
          }}>
          <View
            style={{
              height: '5%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Header
              name="Outlet Details"
              backhandler={() => navigation.goBack()}
            />
          </View>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CInput
            placeholder={'Outlet Name'}
            value={this.state.outletname}
            onChangeText={(text) =>
              this.setState({
                outletname: text,
              })
            }
          />
          <DropDown
            data={this.state.category}
            placeHolder={
              this.state.outletype === '' ? 'Select Outlet Type' : ''
            }
            labelText={this.state.outletype}
            selectedValue={this.state.outletype}
            onChange={(value) => {
              console.log('value dropdown', value);
              this.setState({
                outletype: value.label,
                outletid: value.id,
              });
              console.log(this.state.outletype);
            }}
          />
          <CInput
            placeholder={'State'}
            value={this.state.state}
            onChangeText={(text) =>
              this.setState({
                state: text,
              })
            }
          />
          <CInput
            placeholder={'City'}
            value={this.state.city}
            onChangeText={(text) =>
              this.setState({
                city: text,
              })
            }
          />
        </View>

        <CButton
          btnText="Save"
          onPress={() => {
            console.log('Reset');
            this.updateOutlet();
          }}
          btnStyle={{
            marginTop: 50,
            backgroundColor: Colors.Button_Color,
            borderRadius: 25,
            width: '60%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    display: 'flex',
  },
  arrowStyle: {
    borderColor: 'black',
    borderWidth: 1,
    width: '8%',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
