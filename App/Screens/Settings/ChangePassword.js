import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import CInput from '../../Components/CInput';
import CButton from '../../Components/CButton';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import Header from '../../Components/CustomeHeader/CustomeHeader';
import Profile from 'Images/forget.png';
import * as reg from '../../Constant/Perfix';
import * as auth from 'Services/auth.js';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldpassword: '',
      newpassword: '',
      confirmpassword: '',
      spinner: false,
      token: '',
    };
  }
  async componentDidMount() {
    this.setState({
      token: await AsyncStorage.getItem('@Token'),
    });
  }
  validation = () => {
    if (this.state.oldpassword === '') {
      alert('Please enter your old password');
    } else if (this.state.newpassword === '') {
      alert('Please enter your newpassword');
    } else if (this.state.confirmpassword !== this.state.newpassword) {
      alert("Your Password doesn't match ");
    } else {
      this.makeRequest();
    }
  };
  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });

    const data = new FormData();
    data.append('old_password', this.state.oldpassword);
    data.append('new_password', this.state.newpassword);
    data.append('confirm_password', this.state.confirmpassword);
    auth
      .resetpasssword(this.state.token, data)
      .then((response) => {
        console.log('---!Response!---', response);
        if (response.success === 1) {
          this.setState({
            spinner: false,
          });
          const router = mainStack.setting;
          navigation.navigate(router);
        } else if (response.success === 0) {
          // this.setState({
          //   spinner: false
          // })
          // alert(response.error);
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
  };
  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAwareScrollView
        style={Styles.container}
        contentContainerStyle={{
          // flex: 1,
          flexGrow: 1,

          backgroundColor: Colors.white,
        }}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
            paddingHorizontal: 20,
          }}>
          <View style={{height: '5%', marginTop: 20}}>
            <Header
              name="Change Password"
              backhandler={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              paddingVertical: 20,

              // justifyContent: 'center',
              height: '95%',
            }}>
            <View style={{alignItems: 'center'}}>
              <Image source={Profile} resizeMode="contain" />
            </View>
            <View
              style={{
                marginVertical: 30,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 22,
              }}>
              <Text
                style={[styles.subTitle, {fontSize: 16, textAlign: 'center'}]}>
                Your new password must be different from previous used
                passwords.{' '}
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <CInput
                placeholder={'Old Password'}
                value={this.state.oldpassword}
                onChangeText={(text) =>
                  this.setState({
                    oldpassword: text,
                  })
                }
                SecureTextEntry={true}
              />
              <CInput
                placeholder={'New Password'}
                value={this.state.newpassword}
                onChangeText={(text) =>
                  this.setState({
                    newpassword: text,
                  })
                }
                SecureTextEntry={true}
              />
              <CInput
                placeholder={'Re-type Password'}
                value={this.state.confirmpassword}
                onChangeText={(text) =>
                  this.setState({
                    confirmpassword: text,
                  })
                }
                SecureTextEntry={true}
              />
            </View>
            <View style={{marginVertical: 10}}>
              <CButton
                btnText="Done"
                onPress={() => {
                  console.log('Reset');
                  this.validation();
                }}
                btnStyle={{
                  marginTop: 50,
                  backgroundColor: Colors.Button_Color,
                  borderRadius: 25,
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    // flexDirection: 'column',
    backgroundColor: Colors.white,
    // paddingHorizontal: 25,
    // paddingVertical: 30
  },
});
