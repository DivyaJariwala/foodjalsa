import React, {Component} from 'react';
import {View, Text, SafeAreaView, Image, StyleSheet} from 'react-native';
import Option from '../../Components/Option';
import Header from '../../Components/CustomeHeader/CustomeHeader';
import CommonStyle from '../../Config/CommonStyle';
import Profile from 'Images/profile.png';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import {authStack, settingStack, rootswitch} from '../../Config/Navigator';
import * as auth from '../../Services/auth';
import styles from '../../Config/styles';
import Divider from '../../Components/Divider/Divider';
import Modal from 'react-native-modal';
import * as profile from '../../Services/updatedetails';
import {TouchableOpacity} from 'react-native';
import key from '../../../Assets/Images/key.png';
import store from '../../../Assets/Images/storefront.png';
import poweroff from '../../../Assets/Images/poweroff.png';
import user from '../../../Assets/Images/user.png';
export default class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      spinner: false,
      visible: false,
      name: '',
      email: '',
      letter: '',
    };
  }
  async componentDidMount() {
    this.setState({
      token: await AsyncStorage.getItem('@Token'),
    });
    this.getPersonalDetails();
  }

  getPersonalDetails = () => {
    this.setState({
      spinner: true,
    });
    profile.personaldetails(this.state.token).then(async (response) => {
      console.log('Response', response);
      if (response.success === 1) {
        this.setState({
          name: response.data.outlet_name,
          letter: response.data.avatar_letter,
          email: response.data.email,
          spinner: false,
        });
        console.log('Logout');
      } else if (response.success === 0) {
        this.setState({spinner: false});
        alert(response.error);
      }
    });
  };

  makeRequest = () => {
    console.log('----!LOGOUT!-----');
    const {navigation} = this.props;
    this.setState({
      spinner: true,
      visible: false,
    });
    auth.logout(this.state.token).then(async (response) => {
      console.log('Response', response);
      if (response.success === 1) {
        await AsyncStorage.clear();
        this.setState({spinner: false});
        console.log('Logout');
        // navigation.dispatch(
        //   CommonActions.navigate({
        //     name: 'auth',
        //     params: {},
        //   })
        // );
        const router = rootswitch.auth;
        navigation.navigate(router);
      } else if (response.success === 0) {
        this.setState({spinner: false});
        alert(response.error);
      }
    });
  };
  render() {
    const {navigation} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={({height: '100%'}, CommonStyle.container)}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Loading...'}
            textStyle={{color: '#FFF'}}
          />
          <View
            style={{
              marginVertical: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[styles.Heading, {fontSize: 26, fontWeight: '600'}]}>
              My Account
            </Text>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              {/* <Image
                source={Profile}
                resizeMode="contain"
                style={{height: 150, width: 120}}></Image> */}
              <TouchableOpacity style={[style.avtarplaceholder]}>
                {/* <Image
                  source={{
                    uri: this.state.fileuri,
                  }}
                  style={[style.avtar]}
                /> */}
                <Text
                  style={[
                    {fontSize: 30, fontWeight: 'bold', color: '#EC893E'},
                  ]}>
                  {this.state.letter}
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.Heading,
                  {fontSize: 18, fontWeight: '600', marginTop: 10},
                ]}>
                {this.state.name}{' '}
              </Text>
              <Text
                style={[
                  styles.Heading,
                  {fontSize: 12, fontWeight: '600', color: '#F5B4B2'},
                ]}>
                {this.state.email}{' '}
              </Text>
            </View>
            <View style={{marginTop: 15, marginHorizontal: 20}}>
              <Option
                option="Personal Details"
                name="user"
                rightArror
                image={user}
                Options={() => {
                  const router = settingStack.personaldetails;
                  navigation.navigate(router);
                }}
              />
              <Option
                option="Outlet Details"
                name="home-outline"
                rightArror
                image={store}
                Options={() => {
                  const router = settingStack.outletdetails;
                  navigation.navigate(router);
                }}
              />
              <Option
                option="Change Password"
                name="key"
                rightArror
                image={key}
                Options={() => {
                  const router = settingStack.changepassword;
                  navigation.navigate(router);
                }}
              />
              <Divider />
              <Option
                option="Logout"
                name="logout"
                image={poweroff}
                logout
                Options={() =>
                  this.setState({
                    visible: true,
                  })
                }
              />
            </View>
          </View>
          <Modal isVisible={this.state.visible} style={{alignItems: 'center'}}>
            <View
              style={{
                height: '25%',
                backgroundColor: 'white',
                width: '60%',
                justifyContent: 'center',
                borderRadius: 15,
                shadowColor: '#1F1F1F',
                shadowOffset: {width: 2, height: 1.5},
                shadowOpacity: 1,
                shadowRadius: 4,
                elevation: 5,
                borderWidth: 0,
                // alignItems:'center'
              }}>
              <View
                style={{
                  height: '25%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: 16,
                    // textAlign: 'center',
                  }}>
                  Log out of
                </Text>

                <Text
                  style={{
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: 16,
                    textAlign: 'center',
                  }}>
                  Food Jalsa?
                </Text>
              </View>
              <View style={{justifyContent: 'center'}}>
                <Divider marginBottom={6} />
              </View>
              <TouchableOpacity
                onPress={() => this.makeRequest()}
                style={{
                  height: '15%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#FF3C3D',
                    fontWeight: '400',
                    fontSize: 16,
                    textAlign: 'center',
                  }}>
                  Log Out
                </Text>
              </TouchableOpacity>
              <Divider marginBottom={6} />
              <TouchableOpacity
                style={{
                  height: '15%',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() =>
                  this.setState({
                    visible: false,
                  })
                }>
                <Text
                  style={{
                    color: 'black',
                    fontWeight: '400',
                    fontSize: 16,
                    textAlign: 'center',
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  avtar: {
    position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 10,
  },
  avtarplaceholder: {
    height: 100,
    width: 100,

    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
    marginTop: 10,
  },
});
