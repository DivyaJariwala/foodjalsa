import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Alert,
  Linking,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import code from 'Images/qrcode.png';
import CButton from '../../Components/CButton';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as qrcode from '../../Services/Menu';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import QRCode from 'react-native-qrcode-svg';
import {authStack, mainStack} from '../../Config/Navigator';
import RNFetchBlob from 'rn-fetch-blob';
import Share1 from '../../Components/BottomSheet';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import fonts, {normalize} from '../../Config/fonts';
import {shareOnFacebook, shareOnTwitter} from 'react-native-social-share';
import Share from 'react-native-share';
export default class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qrcount: 0,
      spinner: false,
      token: '',
      qrvalue: '',
      qrcode: '',
      qrstring: '',
      visibel: false,
      concatestring: '',
    };
  }
  async componentDidMount() {
    this.setState({
      spinner: true,
      token: await AsyncStorage.getItem('@Token'),
      qrcode: await AsyncStorage.getItem('@Qrcode'),
      qrstring: await AsyncStorage.getItem('@Qrstring'),
    });

    console.log('qrstring', this.state.qrstring);
    console.log('code', this.state.qrcode);
    console.log('concatestring', this.state.concatestring);

    qrcode
      .Qrcount(this.state.token)
      .then(async (response) => {
        console.log('---!Response!---', response);
        console.log('suvess', response.data);
        if (response.success === 1) {
          this.setState({
            qrcount: response.data.total_scan,
            concatestring: `https://foodjalsa.com/restaurant/${this.state.qrstring}`,
            spinner: false,
          });

          // const router = mainStack.sidebar;
          // navigation.navigate(router);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  }

  checkPermission = async () => {
    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      this.downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download Photos',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          console.log('Storage Permission Granted.');
          this.downloadImage();
        } else {
          // If permission denied then show alert
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };
  downloadImage = () => {
    // Main function to download the image

    // To add the time suffix in filename
    let date = new Date();
    // Image URL which we want to download
    let image_URL = this.state.qrcode;
    // Getting the extention of the file
    let ext = this.getExtention(image_URL);
    ext = '.' + ext[0];
    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const {config, fs} = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        // Related to the Android only
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/image_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          ext,
        description: 'Image',
      },
    };

    config(options)
      .fetch('GET', image_URL)
      .then((res) => {
        // Showing alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        alert('Image Downloaded Successfully.');
      });
  };
  getExtention = (filename) => {
    // To get the file extension
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };
  facebookShare = () => {
    // shareOnFacebook(
    //   {
    //     link: `https://foodjalsa.co/restaurant/ ${this.state.qrstring}`,
    //     //or use image
    //   },
    //   (results) => {
    //     console.log(results);
    //   },
    // );
    const shareOptions = {
      url: this.state.concatestring,
      social: Share.Social.FACEBOOK,

      filename: 'test', // only for base64 file in Android
    };

    Share.shareSingle(shareOptions)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };
  TwintterShare = () => {
    shareOnTwitter(
      {
        link: `https://foodjalsa.com/restaurant/ ${this.state.qrstring}`,
      },
      (results) => {
        console.log(results);
      },
    );
  };
  InstaShare = () => {
    const shareOptions = {
      url: this.state.concatestring,
      social: Share.Social.INSTAGRAM,

      filename: 'test', // only for base64 file in Android
    };

    Share.shareSingle(shareOptions)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };
  whatsappClick = () => {
    const shareOptions = {
      url: this.state.concatestring,
      social: Share.Social.WHATSAPP,

      filename: 'test', // only for base64 file in Android
    };

    Share.shareSingle(shareOptions)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };

  render() {
    const {navigation, isFocused} = this.props;
    // this.setState({
    //   concatestring: `https://foodjalsa.com/resturant/${this.state.qrstring}`,
    // });
    console.log('1', this.state.concatestring);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingVertical: 20,
            paddingHorizontal: 10,
            backgroundColor: 'white',
          }}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Loading...'}
            textStyle={{color: '#FFF'}}
          />
          <Text style={[styles.Heading, {fontSize: 26, fontWeight: '600'}]}>
            Your Outlet QR Code
          </Text>
          <View
            style={{
              marginTop: 20,
            }}>
            <Text>
              <Text
                style={{
                  fontFamily: fonts.Poppins.regular,
                  color: Colors.black,
                  fontSize: normalize(16),
                  fontWeight: '400',
                }}>
                Total Scan Count :{' '}
              </Text>
              <Text
                style={{
                  fontFamily: fonts.Poppins.regular,
                  color: Colors.Button_Color,
                  fontSize: normalize(16),
                  fontWeight: 'bold',
                }}>
                {this.state.qrcount}
              </Text>
            </Text>
          </View>
          {/* <Image source={code} resizeMode="contain"></Image> */}
          <View
            style={{
              marginTop: 15,
              borderColor: 'black',
              borderRadius: 4,
              borderWidth: 1,
              padding: 10,
            }}>
            <QRCode
              //QR code value
              // value={
              //   this.state.qrvalue
              //     ? `https://foodjalsa.com/resturant/${this.state.qrvalue}`
              //    : 'NA'
              // }
              value={this.state.concatestring ? this.state.concatestring : 'NA'}
              //size of QR Code
              size={150}
              //Color of the QR Code (Optional)
              color="black"
              //Background Color of the QR Code (Optional)
              backgroundColor="white"
              //Logo of in the center of QR Code (Optional)
              logo={{
                url: this.state.qrcode,
              }}
              //Center Logo size  (Optional)
              logoSize={25}
              //Center Logo margin (Optional)
              logoMargin={2}
              //Center Logo radius (Optional)
              logoBorderRadius={15}
              //Center Logo background (Optional)
            />
          </View>
          <View
            style={{
              marginTop: 20,
              paddingHorizontal: 12,
              alignItems: 'center',
              height: '5%',
            }}>
            <Text
              style={{
                fontFamily: fonts.Poppins.regular,
                color: Colors.black,
                fontSize: normalize(14),
                fontWeight: '400',
                textAlign: 'center',
              }}>
              https://foodjalsa.com/restaurant/
              {this.state.qrstring}
            </Text>
          </View>
          <View style={{marginTop: 15, height: '5%'}}>
            <Text
              style={{
                fontFamily: fonts.Poppins.regular,
                color: '#BAB9C0',
              }}>
              Here is your menu link
            </Text>
          </View>
          <View style={{height: '10%', width: '100%'}}>
            <CButton
              btnText="Click to view your menu"
              btnStyle={{
                marginTop: 25,
                backgroundColor: Colors.Button_Color,
                borderRadius: 9,
                width: '60%',
                justifyContent: 'center',
                alignItems: 'center',

                // paddingHorizontal: 3,
              }}
              textStyle={{
                fontSize: 12,
                // fontWeight: '600',
                fontFamily: fonts.Poppins.regular,
              }}
              onPress={() => {
                Linking.openURL(this.state.concatestring);
              }}
            />
          </View>
          <View style={Styles.bottonconatiner}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  visibel: !this.state.visibel,
                });
              }}>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <AntDesign name="sharealt" size={30} />
                <Text
                  style={{
                    fontFamily: fonts.Poppins.regular,
                    color: Colors.black,
                    fontSize: normalize(12.5),
                    fontWeight: '400',
                    marginTop: 8,
                  }}>
                  Share Menu Link
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.checkPermission.bind()}>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <MaterialCommunityIcons name="arrow-collapse-down" size={30} />
                <Text
                  style={{
                    fontFamily: fonts.Poppins.regular,
                    color: Colors.black,
                    fontSize: normalize(12.5),
                    fontWeight: '400',
                    marginTop: 8,
                  }}>
                  Download QR Code
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Share1
            visible={this.state.visibel}
            toggleBottomNavigationView={() => {
              this.setState({
                visibel: !this.state.visibel,
              });
            }}
            facebookClick={this.facebookShare.bind()}
            twitterClick={this.TwintterShare.bind()}
            instaClick={this.InstaShare.bind()}
            whatsappClick={this.whatsappClick.bind()}
            // toggleBottomNavigationView={this.toggleBottomNavigationView()}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    // flex: 1,
  },
  bottonconatiner: {
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    alignItems: 'center',
    marginTop: 50,
  },
});
