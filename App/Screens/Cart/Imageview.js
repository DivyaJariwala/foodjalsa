import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import Swiper from 'react-native-swiper';
import AsyncStorage from '@react-native-community/async-storage';
import * as menu from '../../Services/Menu';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Imageview extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      image_file: '',
      outletid: '',
      token: '',
      fileArray: [],
      spinner: false,
      swiper: '',
      currIndex: 0,
    };
  }
  async componentDidMount() {
    this.setState({
      outletid: await AsyncStorage.getItem('@outletid'),
      token: await AsyncStorage.getItem('@Token'),
    });
    this.getMenuFile();
    this.el.scrollBy(2, true);
  }
  getMenuFile = () => {
    this.setState({spinner: true});
    const data = new FormData();
    data.append('outlet_id', this.state.outletid);
    menu
      .menucount(this.state.token, data)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('success', response.success);
        if (response.success === 1) {
          this.setState({
            fileArray: response.data.files,
            spinner: false,
          });

          console.log('Files', this.state.fileArray[1].file);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <Swiper
          showsButtons={false}
          // index={0}
          ref={(el) => (this.loadingComponent.scrollBy(1, true) = el)}
          showsPagination={false}>
          {this.state.fileArray.map((item, index) => (
            <Image
              key={item.id}
              source={{
                uri: item.file,
              }}
              style={{
                height: '100%',
                width: '100%',
              }}
              resizeMode="contain"
            />
          ))}
        </Swiper>
      </View>
    );
  }
}
