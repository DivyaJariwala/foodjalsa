import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Image,
  Alert,
  Modal,
  Pressable,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';
import DocumentPicker from 'react-native-document-picker';
import Pdf from 'react-native-pdf';
import AsyncStorage from '@react-native-community/async-storage';
import * as menu from '../../Services/Menu';
import Styles from '../../Config/styles';
import {FlatList} from 'react-native';
import Rectangle from '../../../Assets/Images/File.png';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../../Config/colors';
import {mainStack} from '../../Config/Navigator';
import {width, height, totalSize} from 'react-native-dimension';

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      multiplefile: [],
      outletid: '',
      token: '',
      fileArray: [],
      showpdf: false,
      filelink: '',
      modelVisible: false,
      selectedIndex: '',
      selectedColor: '',
      showDelete: false,
      selectedItem: [],
      selectType: '',
      fileShow: true,
    };
  }
  async componentDidMount() {
    this.setState({
      outletid: await AsyncStorage.getItem('@outletid'),
      token: await AsyncStorage.getItem('@Token'),
    });
    this.getMenuFile();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.getMenuFile();

      // Use the `this.props.isFocused` boolean
      // Call any action
    }
  }
  getMenuFile = () => {
    this.setState({spinner: true});
    const data = new FormData();
    data.append('outlet_id', this.state.outletid);
    menu
      .menucount(this.state.token, data)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('success', response.success);
        if (response.success === 1) {
          this.setState({
            fileArray: response.data.files,
            spinner: false,
          });
          console.log('Files', this.state.fileArray[0].file);
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };
  selectMultipleFile = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.images],
      });
      for (const res of results) {
        console.log('res : ' + JSON.stringify(res));
        console.log('URI : ' + res.uri);
        console.log('Type : ' + res.type);
        console.log('File Name : ' + res.name);
        console.log('File Size : ' + res.size);
      }

      this.setState({
        multiplefile: results,
      });
      this.makeRequest();
      // this.getMenuFile();
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log('Canceled from multiple doc picker');
      } else {
        console.log('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };
  makeRequest = () => {
    console.log('Multiple==>>', this.state.multiplefile);
    const {navigation} = this.props;
    this.setState({
      spinner: true,
    });
    const data = new FormData();
    data.append('outlet_id', this.state.outletid);
    this.state.multiplefile.forEach((item, i) => {
      data.append('menu_file[]', {
        uri: item.uri,
        type: item.type || `application/image`,
        name: item.name || `filename${i}.jpg`,
      });
    });

    menu
      .upload(this.state.token, data)
      .then(async (response) => {
        console.log('---!Response!--->', response);

        // this.setState({spinner: false});
        if (response.success === 1) {
          this.setState({
            spinner: false,
          });
          this.getMenuFile();
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => console.log('-----!Error!----', error));
  };
  renderItem = (filelink) => {
    console.log('Filelink', filelink.item.file);
    return (
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 10,
        }}>
        <TouchableOpacity
          style={{
            borderWidth: this.state.selectedItem.includes(filelink.item.id)
              ? 2
              : 1,
            borderColor: 'black',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => {
            if (this.state.fileShow === true) {
              this.setState({
                showpdf: true,
                filelink: filelink.item.file,
              });
              const router = mainStack.preview;
              this.props.navigation.navigate(router, {
                file: filelink.item.file,
              });

              // this.showPdfViewr(this.state.filelink);
            } else {
              var array = this.state.selectedItem.slice();
              if (array.includes(filelink.item.id)) {
                array.splice(array.indexOf(filelink.item.id), 1);
                console.log('Array2=========>', array);
              } else {
                array.push(filelink.item.id);
                console.log('Array1=======>', array);
              }
              array.length
                ? this.setState({showDelete: true})
                : this.setState({
                    showDelete: false,
                  });
              console.log('Array lenght', array.length);
              this.setState({
                selectedItem: array,
                selectType: 'Multiple',
              });
              console.log('SelectedItem======>', this.state.selectedItem);
            }
          }}
          onLongPress={() => {
            this.setState({
              // modelVisible: true,
              fileShow: false,
              filelink: filelink.item,
              selectedIndex: filelink.item.id,
              selectedColor: filelink.item.id,
              selectType: 'Single',
              selectedItem: [...this.state.selectedItem, filelink.item.id],
              showDelete: true,
            });

            console.log('filelink=====>', this.state.selectedItem);
          }}>
          <Image
            source={{
              uri: filelink.item.file,
            }}
            // resizeMode="cover"
            style={{height: height(20), width: width(35)}}
          />
        </TouchableOpacity>
      </View>
    );
  };
  // showPdfViewr = (link) => {
  //   console.log('filelink', link);

  //   const source = {
  //     uri: link,
  //     cache: true,
  //   };
  //   return (
  //     <Pdf
  //       source={source}
  //       onLoadComplete={(numberOfPages, filePath) => {
  //         console.log(`number of pages: ${numberOfPages}`);
  //       }}
  //       onPageChanged={(page, numberOfPages) => {
  //         console.log(`current page: ${page}`);
  //       }}
  //       onError={(error) => {
  //         console.log('Error', error);
  //       }}
  //       onPressLink={(uri) => {
  //         console.log(`Link presse: ${uri}`);
  //       }}
  //       style={styles.pdf}
  //     />
  //   );
  // };
  deleteItem = () => {
    this.setState(
      {
        showDelete: false,
      },
      () => {
        if (this.state.selectType === 'Single') {
          const data = new FormData();
          data.append('outlet_id', this.state.outletid);
          data.append('menu_files', this.state.selectedIndex);
          menu
            .deletefile(this.state.token, data)
            .then((response) => {
              console.log('---!Response!---', response);
              console.log('success', response.success);
              if (response.success === 1) {
                this.setState({
                  // count: response.data.total_files,
                  spinner: false,
                  modelVisible: false,
                  selectedItem: [],
                });
                this.getMenuFile();
              } else if (response.success === 0) {
                this.setState({spinner: false, showDelete: false});
                setTimeout(() => {
                  Alert.alert('Oops!', response.message);
                }, 1000);
              }
            })
            .catch((error) => {
              console.log('---!ERROR!---', error);
            });
        } else if (this.state.selectType === 'Multiple') {
          console.log('Selecteditem==>', this.state.selectedItem);
          let array = this.state.selectedItem.toString();
          const data = new FormData();
          data.append('outlet_id', this.state.outletid);
          // array.forEach((item) => {
          data.append('menu_files', array);
          // });

          menu
            .deletefile(this.state.token, data)
            .then((response) => {
              console.log('---!Response!---', response);
              console.log('success', response.success);
              if (response.success === 1) {
                this.setState({
                  // count: response.data.total_files,
                  spinner: false,
                  modelVisible: false,
                  showDelete: false,
                  selectedItem: [],
                });
                this.getMenuFile();
              } else if (response.success === 0) {
                this.setState({
                  spinner: false,
                  showDelete: false,
                  selectedItem: [],
                });
                setTimeout(() => {
                  Alert.alert('Oops!', response.message);
                }, 1000);
              }
            })
            .catch((error) => {
              console.log('---!ERROR!---', error);
            });
        }
      },
    );
  };
  onSubmit = () => {
    var array = [];
    this.state.fileArray.map((item) => {
      console.log('Array', item.id);
      array.push(item.id);
      console.log('Array=====>', array);
    });
    this.setState(
      {
        selectedItem: array,
        selectType: 'Multiple',
      },
      () => {
        console.log('SelectedItem', this.state.selectedItem);
      },
    );
  };
  EmptyListMessage = ({item}) => {
    return (
      // Flat List Item
      <View
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
          No Image
        </Text>
      </View>
    );
  };

  render() {
    const {navigation} = this.props;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            paddingVertical: 30,
            backgroundColor: 'white',
          }}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Loading...'}
            textStyle={{color: '#FFF'}}
          />
          <View
            style={{
              height: '90%',
            }}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                // width: '100%',
                paddingHorizontal: 15,

                // paddingHorizontal: 40,
              }}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={styles.arrowStyle}>
                <Feather name="arrow-left" size={25} color={Color.black} />
              </TouchableOpacity>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '70%',
                }}>
                <Text style={[Styles.H1, {textAlign: 'center'}]}>
                  Menu Files
                </Text>
              </View>
              <View
                style={{
                  width: '15%',
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                }}>
                {this.state.showDelete ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon
                      name="delete"
                      size={30}
                      style={{alignItems: 'center', justifyContent: 'center'}}
                      onPress={() => this.deleteItem()}
                    />
                    <Icon
                      name="select-all"
                      size={30}
                      style={{alignItems: 'center', justifyContent: 'center'}}
                      onPress={() => this.onSubmit()}
                    />
                  </View>
                ) : null}
              </View>
            </View>
            {!this.state.spinner && (
              <View
                style={{
                  marginTop: 20,
                  width: '100%',
                  paddingHorizontal: 25,
                }}>
                <FlatList
                  data={this.state.fileArray}
                  numColumns={2}
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    paddingBottom: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  renderItem={(item) => this.renderItem(item)}
                  ListEmptyComponent={this.EmptyListMessage}
                />
              </View>
            )}
          </View>
          <View
            style={{
              height: '10%',
              justifyContent: 'flex-end',
              flexDirection: 'row',
            }}>
            <TouchableOpacity onPress={() => this.selectMultipleFile()}>
              <Ionicons name="add-circle" size={60} color="#EC893E" />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.centeredView}> */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modelVisible}
            // onRequestClose={() => {
            //   Alert.alert('Modal has been closed.');
            //   setModalVisible(!modalVisible);
            // }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>
                  Are you sure you want to delete this item?
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    paddingTop: 20,
                  }}>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => this.deleteItem()}>
                    <Text style={styles.textStyle}>Yes</Text>
                  </Pressable>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() =>
                      this.setState({
                        modelVisible: false,
                        selectedColor: '',
                      })
                    }>
                    <Text style={styles.textStyle}>No</Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  pdf: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  arrowStyle: {
    // width: '20%',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalView: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 12,
    paddingHorizontal: 2,
    paddingVertical: 6,
    marginHorizontal: 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalText: {
    textAlign: 'center',
    fontSize: 20,
  },
  textStyle: {
    fontSize: 18,
  },
});
