import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  Pressable,
  TouchableOpacity,
  Alert,
} from 'react-native';
import styles from '../../Config/styles';
import Colors from '../../Config/colors';
import {authStack, mainStack} from '../../Config/Navigator';
import Background from '../../../Assets/Images/FOOD.png';
import DropDownPicker from 'react-native-dropdown-picker';
import fonts, {normalize} from '../../Config/fonts';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import * as filecount from '../../Services/Menu';
import watermark from '../../../Assets/Images/watermark.png';
export default class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      spinner: false,
      outletid: '',
      count: '',
    };
  }
  async componentDidMount() {
    this.setState({
      token: await AsyncStorage.getItem('@Token'),
      outletid: await AsyncStorage.getItem('@outletid'),
    });
    this.makeRequest();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.makeRequest();
      //Put your Data loading function here instead of my this.loadData()
    });
  }
  // componentWillUnmount() {
  //   this.willFocusSubscription.remove();
  // }
  makeRequest = () => {
    const {navigation} = this.props;
    this.setState({spinner: true});
    const data = new FormData();
    data.append('outlet_id', this.state.outletid);
    filecount
      .menucount(this.state.token, data)
      .then((response) => {
        console.log('---!Response!---', response);
        console.log('success', response.success);
        if (response.success === 1) {
          this.setState({
            count: response.data.total_files,
            spinner: false,
          });
        } else if (response.success === 0) {
          this.setState({spinner: false});
          setTimeout(() => {
            Alert.alert('Oops!', response.message);
          }, 1000);
        }
      })
      .catch((error) => {
        console.log('---!ERROR!---', error);
      });
  };

  render() {
    const {navigation} = this.props;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFF'}}
        />
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            // justifyContent:'center',
            paddingVertical: 30,
            paddingHorizontal: 10,
            backgroundColor: 'white',
          }}>
          <Text style={[styles.Heading, {fontSize: 26, fontWeight: '600'}]}>
            Home
          </Text>
          <TouchableOpacity
            style={Styles.boxWithShadow}
            onPress={() => {
              const router = mainStack.menu;
              navigation.navigate(router);
            }}>
            <Text
              style={{
                textAlign: 'center',
                // fontFamily: fonts.Poppins.regular,
                color: Colors.black,
                fontSize: normalize(13),
                fontWeight: '400',
              }}>
              MENU ITEMS
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: '#EC893E',
                // fontFamily: fonts.Poppins.regular,
                fontSize: normalize(13),
                fontWeight: 'bold',
              }}>
              {this.state.count}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <Image source={watermark} resizeMode="cover" />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    // flex: 1,
  },
  boxWithShadow: {
    shadowColor: '#1F1F1F',
    shadowOffset: {width: 2, height: 4},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 5,
    borderWidth: 0,
    borderRadius: 3,
    padding: 1.3,
    width: '90%',
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    marginTop: 20,
    height: '8%',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    paddingHorizontal: 10,
  },
});
