import React, {Component} from 'react';
import {Text, View, StyleSheet, SafeAreaView, Image} from 'react-native';
import {rootswitch, mainStack} from '../Config/Navigator';
import Color from '../Config/colors';
import Background from '../../Assets/Images/FOOD.png';
import BottomBackground from '../../Assets/Images/splash.png';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

export default class Loading extends Component {
  componentDidMount() {
    this._bootstrapAsync();
    SplashScreen.hide();
  }
  _bootstrapAsync = async () => {
    const {navigation} = this.props;
    const userToken = await AsyncStorage.getItem('@Token');

    console.log(userToken);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.

    setTimeout(() => {
      this.setState({visible: true});
      const router = userToken ? mainStack.home : rootswitch.auth;
      navigation.replace(router);
    });
  };
  render() {
    return <SafeAreaView style={styles.container}></SafeAreaView>;
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    height: '100%',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
