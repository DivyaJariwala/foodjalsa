/* eslint-disable no-return-assign */
/* eslint-disable max-len */
/* eslint-disable no-lone-blocks */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Platform,
  Dimensions,
  ScrollView,
  Picker,
} from 'react-native';
import FIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
// import MultiSelect from '../libs/react-native-multiple-select/index';
import {RFValue} from 'react-native-responsive-fontsize';
import colors from '../Config/colors';
import settings from '../Config/settings';
import {FORTAB} from '../Config/MQ';
import fonts from '../Config/fonts';

const IOS = Platform.OS === 'ios';
// Data like this
// const dData = [
//   'Javascript',
//   'Go',
//   'Java',
//   'Kotlin',
//   'C++',
//   'C#',
//   'PHP',
// ];

// How to use
{
  /* <DropDown
  containerStyle={styles.dropdownStyle} // change as requirement
  placeholder="PlaceholderText"
  labelText="LabelTitle" // This is for label in left side
  data={startData}
  value={selectedValue}
  onChange={(value) => {
    this.setState({ selectedValue: value });
  }}
/> */
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: FORTAB ? 70 : 50,
    // borderTopWidth: 1,
    borderTopColor: '#ddd',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    width:'90%'
  },
  labelView: {
    justifyContent: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    // backgroundColor: '#f5f8fe',
  },
  labelTextStyle: {
    color: '#333333',
    // fontSize: normalize(FORTAB ? 11 : 13),
  },
  valueTextStyle: {
    flex: 1,
    color: '#333333',
    paddingLeft: IOS ? 0 : 3,
    fontSize: RFValue(14),
  },
  dropdownView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: IOS ? 'flex-start' : 'center',
    justifyContent: 'space-between',
    paddingVertical: IOS ? 0 : 5,
    paddingBottom: IOS ? 5 : 0,
  },
});

class DropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      iosModal: false,
      dataValue: {},
      selectedValue: '',
      selectedItems: [],
      orientation: 1,
    };
  }

  closeModal = () => {
    this.setState({
      modalVisible: false,
      iosModal: false,
    });
  };

  openModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  isIphoneX = () => {
    const dimen = Dimensions.get('window');
    return (
      Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      (dimen.height === 812 ||
        dimen.width === 812 ||
        dimen.height === 896 ||
        dimen.width === 896)
    );
  };
  // onSelectedItemsChange(selectedItems) {
  //   const {onChangeInput} = this.props;
  //   this.setState({selectedItems}, () => {
  //     if (onChangeInput) {
  //       onChangeInput(selectedItems);
  //     }
  //   });
  // }
  renderMultiSelect() {
    const {
      label,
      dropdownData,
      leftIcon,
      placeholder,
      selectedItems,
      onChangeInput,
    } = this.props;
    return (
      <View style={{flex: 1}}>
        {/* <MultiSelect
          // hideTags
          items={dropdownData}
          uniqueKey="value"
          ref={component => {
            this.multiSelect = component;
          }}
          onSelectedItemsChange={i => onChangeInput(i)}
          selectedItems={selectedItems}
          selectText={label}
          renderLeftIcon={() => (
            <View style={styles.leftIconView}>
              <Icon name={'caret-down'} style={styles.leftIcon} />
            </View>
          )}
          fontSize={RFValue(14)}
          searchInputPlaceholderText={placeholder}
          onChangeInput={text => console.log(text)}
          fontFamily={fonts.SFUIText.Regular}
          itemFontFamily={fonts.SFUIText.Regular}
          selectedItemFontFamily={fonts.SFUIText.Semibold}
          altFontFamily={fonts.SFUIText.Regular}
          tagRemoveIconColor={'#262b38'}
          tagBorderColor={colors.lightGreen}
          tagTextColor={'#262b38'}
          selectedItemTextColor={'#262b38'}
          selectedItemIconColor={colors.lightGreen}
          itemTextColor="#262b38"
          displayKey="label"
          searchInputStyle={{color: '#CCC'}}
          submitButtonColor={colors.lightGreen}
          submitButtonText="Done"
          textColor="#262b38"
          dropdownStyles={styles.container}
        /> */}
        {/* <View>
          {this.multiSelect
            ? this.multiSelect.getSelectedItemsExt(selectedItems)
            : null}
        </View> */}
      </View>
    );
  }
  renderSingleItem = () => {
    const {
      containerStyle,
      placeHolder,
      value,
      data,
      labelText,
      ios,
      cancelButton,
      confirmButton,
      labelViewStyle,
      containViewStyle,
      valueStyle,
      rightIcon,
      iconSize,
      iconStyle,
      labelStyle,
      placeHolderTextColor,
      atoll,
      disabled,
      placeholderStyle,
    } = this.props;
    const {modalVisible, selectedValue, dataValue} = this.state;

    const iosStyles = {
      container: {
        flex: 1,
        justifyContent: 'flex-end',
        zIndex: 999,
      },
      content: {
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
      confirmButtonView: {
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 10,
        backgroundColor: '#FFF',
        borderTopWidth: 1,
        borderTopColor: 'rgba(165,165,165,0.2)',
        paddingVertical: 15,
      },
      confirmButtonText: {
        fontWeight: '500',
        fontSize: 18,
        textAlign: 'center',
        color: 'rgba(0,122,255,1)',
      },
      cancelButton: {
        marginVertical: 10,
      },
      cancelButtonView: {
        marginHorizontal: 15,
        marginBottom: this.isIphoneX() ? 50 : 15,
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
      },
      cancelButtonText: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        color: 'rgba(0,122,255,1)',
      },
      titleView: {
        padding: 12,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(165,165,165,0.2)',
      },
      titleText: {
        fontWeight: '500',
        fontSize: 14,
        textAlign: 'center',
        color: '#bdbdbd',
      },
    };

    return (
      <View style={[styles.container, containerStyle]}>
        {labelText ? (
          <View style={[styles.labelView, labelViewStyle]}>
            <Text
              allowFontScaling={false}
              style={[styles.labelTextStyle, labelStyle]}>
              {labelText}
            </Text>
          </View>
        ) : null}
        <TouchableOpacity
          style={[styles.dropdownView, containViewStyle]}
          activeOpacity={disabled ? 1 : 0.5}
          onPress={() =>
            disabled
              ? () => null
              : IOS
              ? this.setState({
                  iosModal: true,
                  selectedValue: _.isEmpty(value)
                    ? !_.isEmpty(data)
                      ? data[0].label
                      : ''
                    : value.label,
                  dataValue: _.isEmpty(value)
                    ? !_.isEmpty(data)
                      ? data[0]
                      : {}
                    : value,
                })
              : this.openModal()
          }>
          <Text
            numberOfLines={1}
            allowFontScaling={false}
            style={[
              styles.valueTextStyle,
              value.label
                ? atoll
                  ? {color: colors.primary}
                  : {}
                : {color: placeHolderTextColor},
              valueStyle,
            ]}>
            {_.isObject(value) && !_.isEmpty(value)
              ? value.label
              : _.isEmpty(selectedValue)
              ? placeHolder
              : !_.isEmpty(data)
              ? data[0].label
              : placeHolder}
          </Text>
          {rightIcon === 'caret-down' ? (
            <Icon
              name={rightIcon}
              size={iconSize}
              style={[{color: '#262b38'}, iconStyle]}
            />
          ) : (
            <FIcon
              name={rightIcon}
              size={iconSize}
              style={[{color: '#262b38'}, iconStyle]}
            />
          )}
        </TouchableOpacity>
        <Modal
          animationType="fade"
          transparent
          visible={modalVisible}
          onRequestClose={() => this.closeModal()}
          style={{flex: 1}}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={this.closeModal}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'rgba(0,0,0,0.6)',
              padding: 30,
            }}>
            <View
              style={{
                width: '100%',
                backgroundColor: '#FFF',
                borderRadius: 5,
              }}>
              {/* <View
                style={{
                  padding: 15,
                  flex: 1,
                  borderBottomWidth: 1,
                  borderBottomColor: '#ddd',
                }}
              >
                <Text>{placeHolder}</Text>
              </View> */}
              <ScrollView
                contentContainerStyle={{
                  width: '100%',
                }}
                style={{width: '100%'}}
                showsVerticalScrollIndicator={false}>
                {_.isArray(data) && data.length > 0
                  ? data.map((item, index) => (
                      <TouchableOpacity
                        key={`dropDown_${index}`}
                        activeOpacity={0.5}
                        style={{
                          padding: 15,
                          flex: 1,
                          borderBottomWidth:
                            !_.isEmpty(data) && data.length - 1 === index
                              ? 0
                              : 1,
                          borderBottomColor: '#ddd',
                        }}
                        onPress={() => this.setData(item)}>
                        <Text style={{fontSize: 14}}>{item.label}</Text>
                      </TouchableOpacity>
                    ))
                  : null}
              </ScrollView>
            </View>
          </TouchableOpacity>
        </Modal>
        <Modal
          style={{flex: 1}}
          visible={this.state.iosModal}
          animationType="none"
          transparent>
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(0,0,0,0.6)',
            }}>
            <View style={iosStyles.container}>
              <View style={iosStyles.content}>
                <View style={iosStyles.titleView}>
                  <Text style={[iosStyles.titleText, ios.titleStyle]}>
                    {placeHolder}
                  </Text>
                </View>
                <Picker
                  selectedValue={
                    selectedValue === null
                      ? !_.isEmpty(data)
                        ? data[0].label
                        : 0
                      : selectedValue
                  }
                  style={{maxHeight: 200, overflow: 'hidden'}}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({
                      dataValue: {value: itemIndex + 1, label: itemValue},
                      selectedValue: itemValue,
                    });
                  }}>
                  {data.map((val, index) => (
                    <Picker.Item
                      key={`item-${index}`}
                      label={val.label}
                      value={val.label}
                    />
                  ))}
                </Picker>
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => this.setData(dataValue)}>
                  <View
                    style={[
                      iosStyles.confirmButtonView,
                      {
                        opacity:
                          selectedValue !== null
                            ? selectedValue !== this.props.value
                              ? 1
                              : 0.1
                            : 1,
                      },
                    ]}>
                    <Text style={iosStyles.confirmButtonText}>
                      {confirmButton}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={iosStyles.cancelButton}>
                <TouchableOpacity activeOpacity={0.9} onPress={this.closeModal}>
                  <View style={iosStyles.cancelButtonView}>
                    <Text style={iosStyles.cancelButtonText}>
                      {cancelButton}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  };

  setData = (data) => {
    const {onChange} = this.props;
    onChange(data);
    this.closeModal();
  };

  render() {
    const {multiSelect} = this.props;
    // return multiSelect ? this.renderMultiSelect() : this.renderSingleItem();
    return this.renderSingleItem();
  }
}
DropDown.propTypes = {
  containerStyle: PropTypes.objectOf(PropTypes.any),
  data: PropTypes.arrayOf(PropTypes.any),
  items: PropTypes.arrayOf(PropTypes.any),
  placeHolder: PropTypes.string,
  value: PropTypes.string,
  labelText: PropTypes.string,
  onChange: PropTypes.func,
  ios: PropTypes.objectOf(PropTypes.any),
  labelViewStyle: PropTypes.objectOf(PropTypes.any),
  containViewStyle: PropTypes.objectOf(PropTypes.any),
  valueStyle: PropTypes.objectOf(PropTypes.any),
  cancelButton: PropTypes.string,
  confirmButton: PropTypes.string,
  rightIcon: PropTypes.string,
  iconSize: PropTypes.number,
  iconStyle: PropTypes.objectOf(PropTypes.any),
  labelStyle: PropTypes.objectOf(PropTypes.any),
  placeholderStyle: PropTypes.any,
  placeHolderTextColor: PropTypes.string,
  atoll: PropTypes.bool,
  disabled: PropTypes.bool,
  multiSelect: PropTypes.bool,
};

DropDown.defaultProps = {
  containerStyle: {},
  placeHolder: '',
  value: {},
  data: [],
  items: [],
  onChange: () => null,
  labelText: '',
  ios: {
    duration: 330,
    overlayColor: 'rgba(0,0,0,0.3)',
  },
  cancelButton: 'Cancel',
  confirmButton: 'Confirm',
  labelViewStyle: {},
  containViewStyle: {},
  valueStyle: {},
  rightIcon: 'chevron-down',
  iconSize: 25,
  iconStyle: {},
  labelStyle: {},
  placeHolderTextColor: '#bac3d2',
  atoll: false,
  disabled: false,
  placeholderStyle: {
    fontSize: RFValue(14),
    color: '#c1c8d3',
  },
  multiSelect: false,
};

export default DropDown;
