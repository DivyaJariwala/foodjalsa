import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Keyboard} from 'react-native';
import {height, totalSize} from 'react-native-dimension';
import Colors from 'Config/colors';
import Styles from './Styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {mainStack} from '../../Config/Navigator';
import styles from 'Config/styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Divider from '../../Components/Divider/Divider';
import {connect} from 'react-redux';
import Action from '../../Redux/action';

const Bottomtabcomp = ({navigation, index, tabIndex}) => {
  const {ItemStyle} = Styles;
  return (
    <View
      style={{
        backgroundColor: 'white',
      }}>
      <Divider />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'flex-end',
          //width: '100%',
          backgroundColor: 'white',
          justifyContent: 'space-around',
          // justifyContent:''
        }}>
        <TouchableOpacity
          onPress={() => {
            tabIndex(0);
            const router = mainStack.home;
            navigation.navigate(router);
          }}
          style={[ItemStyle]}>
          <Ionicons
            name="home-outline"
            size={totalSize(3.5)}
            color={index === 0 ? Colors.Button_Color : Colors.lightGrey}
            // color={Colors.Header_BackgroundColor}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            tabIndex(1);

            const router = mainStack.cart;
            navigation.navigate(router);
          }}
          style={[ItemStyle]}>
          <MaterialIcons
            name="qr-code-scanner"
            size={totalSize(3.5)}
            color={index === 1 ? Colors.Button_Color : Colors.lightGrey}
            // color={Colors.Header_BackgroundColor}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            tabIndex(2);

            const router = mainStack.setting;
            navigation.navigate(router);
          }}
          style={[ItemStyle]}>
          <Feather
            name="user"
            size={totalSize(3.5)}
            color={index === 2 ? Colors.Button_Color : Colors.lightGrey}
            // color={Colors.Header_BackgroundColor}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  if (state === undefined) return {};
  return {
    index: state.index,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    tabIndex: (index) => dispatch(Action.tabIndex(index)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Bottomtabcomp);
