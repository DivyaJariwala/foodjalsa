import {StyleSheet} from 'react-native';
import {height} from 'react-native-dimension';
import Colors from 'Config/colors';

export default StyleSheet.create({
  ItemStyle: {
    height: height(8),
    // width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
