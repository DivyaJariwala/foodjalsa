import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import FontFamily from 'Config/fonts.js';
import Color from 'Config/colors.js';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {TouchableOpacity} from 'react-native';
import fonts from '../../Config/fonts';

const CustomeHeader = (props) => {
  return (
    <View style={styles.container}>
      <View style={{width: '20%'}}>
        <TouchableOpacity onPress={props.backhandler}>
          <Feather name="arrow-left" size={25} color={Color.black} />
        </TouchableOpacity>
      </View>

      <View style={{width: '60%'}}>
        <Text style={styles.headername}>{props.name}</Text>
      </View>
      <View
        style={{
          width: '20%',
          justifyContent: 'flex-end',
          flexDirection: 'row',
        }}>
        {props.righticon ? (
          <Text style={{color: Color.blue1, fontSize: RFValue(16)}}>
            {props.rightMenu}
          </Text>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
  },
  headername: {
    textAlign: 'center',
    fontSize: RFValue(21),
    fontWeight: 'bold',
    fontFamily: fonts.Poppins.regular,
  },
});

export default CustomeHeader;
