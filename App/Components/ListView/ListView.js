import React from 'react';
import {View, Text, Image, Touchable} from 'react-native';
import {height, width} from 'react-native-dimension';
import Books from 'Images/Books.jpg';
import Style from './Style';
import {TouchableOpacity} from 'react-native-gesture-handler';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Colors from 'Config/colors';
import styles from 'Config/styles';

const {BoxStyle} = Style;
const ListView = (props) => {
  return (
    <TouchableOpacity onPress={() => props.gotoDetails}>
      <View style={[BoxStyle]}>
        <View
          style={{
            width: '30%',
            borderWidth: 0.4,
            borderRadius: 8,
            borderColor: 'black',
          }}>
          <Image
            source={Books}
            resizeMode="contain"
            style={{
              height: height(15),
              width: width(25),
              justifyContent: 'center',
              alignItems: 'center',
            }}></Image>
        </View>
        <View style={{width: '60%', marginLeft: 10, marginVertical: 10}}>
          <Text style={{fontSize: 17, color: 'black'}}>Spiral NootBook</Text>

          <Text style={[styles.H3, {fontSize: 14,marginTop:5}]}>Classmate Notebook</Text>
          <Text style={([styles.H6], {fontWeight: 'bold', color: 'black',marginTop:5})}>
            RS. 90
          </Text>
          {props.productCount ? (
            <View style={{flexDirection: 'row'}}>
              <Text>+</Text>
              <Text>1</Text>
              <Text>-</Text>
            </View>
          ) : null}
        </View>
        <View style={{width: '10%'}}>
          {props.delete ? (
            <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
              <EvilIcons name="close" size={20} />
            </View>
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ListView;
