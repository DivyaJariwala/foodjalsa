import {StyleSheet} from 'react-native';
import {height} from 'react-native-dimension';
import Colors from 'Config/colors';

export default StyleSheet.create({
  BoxStyle: {
    flexDirection: 'row',
    shadowColor: Colors.Shadow_Color,
    shadowOffset: {width: 2, height: 4},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 5,
    borderWidth: 0,
    borderRadius: 6,
    // padding: height(1.3),
    width: '100%',
    
    backgroundColor: Colors.white,
    marginVertical: height(2.5),
  },
});
