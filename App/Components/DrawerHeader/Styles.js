import {StyleSheet, Platform} from 'react-native';
import {height, width, totalSize} from 'react-native-dimension';
import Colors from 'Config/colors';
import * as FontFamily from 'Config/fonts';

export default StyleSheet.create({
  MainContainer: {
    width: '100%',
    marginTop: Platform.OS === 'ios' ? height(3) : 0,
  },

  headerView: {
    paddingHorizontal: width(6),
    height: height(10),
    justifyContent: 'center',
  },

  loginView: {
    marginTop: height(3),
    height: height(30),
    justifyContent: 'center',
    alignItems: 'center',
  },

  loginText: {
    marginTop: height(1.5),
    textAlign: 'center',
    // fontFamily: FontFamily.overPass.bold,
    // color: Colors.Primary_BackgroundColor,
    fontSize: totalSize(2),
  },
});
