import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  ImageBackground,
} from 'react-native';
import Styles from './Styles';
import {totalSize, height} from 'react-native-dimension';
import Profile from 'Images/profile.png';
const {
  MainContainer,
  headerView,
  signupContainer,
  signupText,
  loginView,
  loginText,
} = Styles;

const DrawerHeader = ({navigateToProfile, navigateToSignup}) => {
  return (
    <View style={MainContainer}>
      <TouchableOpacity onPress={() => navigateToProfile()} style={loginView}>
        <ImageBackground
          source={Profile}
          imageStyle={{borderRadius: height(5)}}
          style={{height: height(20), width: height(20), marginTop: height(6)}}
        />
        <Text style={loginText}>Drawer</Text>
      </TouchableOpacity>
    </View>
  );
};

export default DrawerHeader;
