import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import _ from 'lodash';
// import DatePicker from 'react-native-datepicker';
// import Icon from '../libs/react-native-vector-icons/FontAwesome';
// import MIcon from '../libs/react-native-vector-icons/MaterialCommunityIcons';
// import moment from 'moment';
import commonStyles from '../Config/styles';
import colors from '../Config/colors';
import settings from '../Config/settings';
import { FORTAB } from '../Config/MQ';
import fonts from '../Config/fonts';

const dH = FORTAB ? 60 : 45;

const { borderIssue } = settings;

const styles = StyleSheet.create({
  container: {
    height: dH,
    width: '100%',
    padding: 5,
    marginVertical: FORTAB ? 15 : 5,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 3,
    borderColor: 'black',
    borderBottomWidth: 1,
  },
  containerFull: {
    height: dH * 3,
    width: '100%',
    padding: 5,
    marginVertical: FORTAB ? 10 : 5,
    flexDirection: 'row',
    backgroundColor: colors.semiTransparentWhite,
    borderColor: '#ddd',
    borderBottomWidth: 1,
  },
  input: {
    // color: '#000',
    flex: 1,
    fontSize: FORTAB ? 20 : 14,
    paddingTop: 0,
    paddingBottom: 0,
    textAlign: 'left',
    borderWidth: 0,
    color: 'black',
    fontFamily: fonts.Poppins.regular,

  },
  dateText: {
    color: '#FFF',
    fontSize: FORTAB ? 20 : 12,
    textAlign: 'left',
  },
  datePickerCont: {
    height: 30,
    width: '100%',
    justifyContent: 'center',
  },
  leftIconView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightIconView: {
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    borderBottomRightRadius: 3,
    borderTopRightRadius: 3,
    marginTop: -5,
    marginRight: -5,
  },
  leftIcon: {
    color: '#ccc',
    marginTop: -2,
    fontSize: FORTAB ? 18 : 13,
  },
  rightIcon: {
    color: '#ffffff',
    marginTop: -2,
    fontSize: FORTAB ? 20 : 15,
  },
  rightBtn: {
    width: dH,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    flex: 1,
    borderColor: '#c0c0c0',
  },
  btnStyle: {
    marginTop: 3,
    width: 100,
    backgroundColor: colors.primary,
  },
});
const datePickerStyle = StyleSheet.create({
  dateInput: {
    borderWidth: 0,
    flex: 1,
    paddingTop: 0,
    paddingBottom: 0,
    height: dH - (FORTAB ? 20 : 10),
    alignItems: 'flex-start',
  },
  placeholderText: {
    color: 'black',
    fontSize: 13,
    textAlign: 'left',
  },
  dateText: {
    color: '#323232',
    fontSize: 13,
    textAlign: 'left',
  },
  btnTextConfirm: {
    color: '#000',
  },
});

class CInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      date: '2016-05-15',
    };
  }

  onFocus() {
    this.setState({ focused: true }, () => {
      if (this.props.onFocus) {
        this.props.onFocus();
      }
    });
  }

  onBlur() {
    this.setState({ focused: false }, () => {
      if (this.props.onBlur) {
        this.props.onBlur();
      }
    });
  }

  getContainerStyle() {
    const { focused } = this.state;
    const style = {};
    if (focused) {
      if (borderIssue) {
        style.backgroundColor = colors.brandColor;
      } else {
        style.borderColor = colors.brandColor;
        style.borderWidth = 0;
      }
    }
    if (borderIssue) {
      style.borderWidth = 0;
    }
    return style;
  }

  focus() {
    if (this.input !== undefined && this.input !== null) {
      if (this.props.datePicker) {
        this.input.onPressDate();
      } else {
        this.input.focus();
      }
    }
  }

  render() {
    const {
      inputStyle,
      leftIcon,
      textArea,
      inputContainerStyle,
      placeholderColor,
      placeholderTextColor,
      onChangeText,
      onClickRighticon,
      onSubmitEditing,
      RightBtnText,
      datePicker,
      minDate,
      rightIcon,
      leftIconStyle,
      value,
      maxLength,
      mode,
      leftLabel,
      rightLabel,
      leftLabelStyle,
      rightLabelStyle,
      rightIconStyle,
      placeholder,
      onClickRightlable,
      wrapperStyle,
      rightIconView,
      location,
      focus,
      contextMenuHidden,
      SecureTextEntry,
      KeyBoardType

    } = this.props;
    console.log('this.props', this.props);
    return (
      <View style={[{ width: '90%' }, wrapperStyle]}>
        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
          {leftLabel ? (
            <Text
              style={[
                commonStyles.semiBold,
                { color: '#c1c8d3', marginLeft: 3 },
                leftLabelStyle,
              ]}>
              {leftLabel}
            </Text>
          ) : null}
          {rightLabel ? (
            <TouchableOpacity
              onPress={this.props.onClickRightlable}
              activeOpacity={0.9}>
              <Text
                style={[
                  commonStyles.semiBold,
                  { color: '#c1c8d3', marginLeft: 3 },
                  rightLabelStyle,
                ]}>
                {rightLabel}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={[
            textArea ? styles.containerFull : styles.container,
            this.getContainerStyle(),
            inputContainerStyle,
          ]}>
          {leftIcon ? (
            <View style={styles.leftIconView}>
              {/* <MIcon name={leftIcon} style={[styles.leftIcon, leftIconStyle]} /> */}
            </View>
          ) : null}
          <TextInput
            selectionColor={colors.primary} // change the cursor color here
            {...this.props}
            ref={(o) => {
              this.input = o;
            }}
            multiline={textArea}
            maxLength={maxLength}
            numberOfLines={textArea ? 4 : 1}
            placeholderTextColor={placeholderTextColor}
            contextMenuHidden={contextMenuHidden}
            underlineColorAndroid="#0000"
            onFocus={location ? focus : () => this.onFocus}
            onBlur={() => this.onBlur()}
            autoCapitalize="none"
            placeholder={placeholder}
            secureTextEntry={SecureTextEntry}
            autoCorrect={false}
            keyboardType={KeyBoardType}
            style={[
              styles.input,
              inputStyle,
              textArea ? { textAlignVertical: 'top' } : null,
            ]}
          />
          {/* )} */}
          {rightIcon ? (
            <TouchableOpacity
              onPress={this.props.onClickRighticon}
              activeOpacity={0.9}>
              <View style={[styles.rightIconView, rightIconView]}>
                {/* <MIcon
                  name={rightIcon}
                  style={[styles.rightIcon, rightIconStyle]}
                /> */}
              </View>
            </TouchableOpacity>
          ) : null}
          {/* { !_.isEmpty(RightBtnText) ? (
            <View style={styles.rightBtn}>
              <CButton
                titleStyle={[styles.title, commonStyles.txtCenter,
                      { marginHorizontal: 10, fontSize: 15 }]}
                buttonStyle={[styles.btnStyle]}
                title={RightBtnText}
              />
            </View>
          ) : (null) } */}
        </View>
      </View>
    );
  }
}

CInput.propTypes = {
  inputStyle: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.array,
  ]),
  onFocus: PropTypes.func,
  onClickRighticon: PropTypes.func,
  onClickRightlable: PropTypes.func,
  onBlur: PropTypes.func,
  datePicker: PropTypes.bool,
  onSubmitEditing: PropTypes.func,
  contextMenuHidden: PropTypes.func,
  onChangeText: PropTypes.func,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  leftIcon: PropTypes.string,
  rightIcon: PropTypes.string,
  RightBtnText: PropTypes.string,
  minDate: PropTypes.string,
  textArea: PropTypes.bool,
  placeholderColor: PropTypes.string,
  maxLength: PropTypes.string,
  leftIconStyle: PropTypes.objectOf(PropTypes.any),
  rightIconStyle: PropTypes.objectOf(PropTypes.any),
  inputContainerStyle: PropTypes.objectOf(PropTypes.any),
  rightIconView: PropTypes.objectOf(PropTypes.any),
  mode: PropTypes.string,
  leftLabel: PropTypes.string,
  rightLabel: PropTypes.string,
  leftLabelStyle: PropTypes.objectOf(PropTypes.any),
  rightLabelStyle: PropTypes.objectOf(PropTypes.any),
  location: PropTypes.bool,
  focus: PropTypes.func,
};

CInput.defaultProps = {
  inputStyle: null,
  onFocus: null,
  onClickRighticon: null,
  onClickRightlable: null,
  onBlur: null,
  datePicker: false,
  onSubmitEditing: null,
  onChangeText: null,
  contextMenuHidden: false,
  placeholder: '',
  leftIcon: null,
  rightIcon: null,
  RightBtnText: '',
  textArea: false,
  inputContainerStyle: {},
  rightIconView: {},
  leftIconStyle: null,
  rightIconStyle: null,
  placeholderColor: null,
  placeholderTextColor: '#000000',
  minDate: '',
  value: '',
  maxLength: 1000,
  mode: 'date',
  leftLabel: '',
  rightLabel: '',
  leftLabelStyle: {},
  rightLabelStyle: {},
  location: false,
  focus: null,
};

export default CInput;
