import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
// import LinearGradient from 'react-native-linear-gradient';
import {RFValue} from 'react-native-responsive-fontsize';
// import IonIcon from '../libs/react-native-vector-icons/Ionicons';
// import FAIcon from '../libs/react-native-vector-icons/FontAwesome';
import common, {gradient} from '../Config/styles';
import {FORTAB} from '../Config/MQ';
// import {Icon} from '../config/icons';
import fonts, {normalize} from '../Config/fonts';

// const { width } = Dimensions.get('window');
const dH = FORTAB ? 60 : 50;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: dH,
    width: '100%',
    // padding: FORTAB ? 0 : 5,
    // marginVertical: FORTAB ? 10 : 5,
    backgroundColor: '#FFCC2A',
    shadowColor: '#1F1F1F',
    shadowOffset: {width: 3, height: 1.5},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 5,
    borderWidth: 0,

    padding: 1.3,
  },
  btnCont: {
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    paddingHorizontal: dH / 2,
  },
  btnText: {
    color: '#FFF',
    fontSize: FORTAB ? RFValue(40) : RFValue(18),
    fontWeight: '600',
    fontFamily: fonts.Poppins.regular,
    // fontFamily: colors.fonts.montserrat.regular,
  },
  linearGradient: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },

  leftIcon: {
    color: '#FFF',
    fontSize: FORTAB ? 30 : 20,
    marginHorizontal: 8,
  },
  disableBtn: {
    backgroundColor: '#252525',
  },
});

export default class CButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  componentDidMount() {}

  renderIcon() {
    const {iconProvider, leftIcon, leftIconSty} = this.props;
    // if (iconProvider === 'ionicons') {
    //   return <IonIcon name={leftIcon} style={(styles.leftIcon, leftIconSty)} />;
    // } else if (iconProvider === 'fontawesome') {
    //   return <FAIcon name={leftIcon} style={(styles.leftIcon, leftIconSty)} />;
    // }
    // return <Icon name={leftIcon} style={(styles.leftIcon, leftIconSty)} />;
  }

  render() {
    const {
      btnText,
      load,
      onPress,
      btnStyle,
      btnPadding,
      textStyle,
      leftIcon,
      disable,
      normal,
      gradientBtnColor,
    } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={disable ? 1 : 0.6}
        style={[
          !disable ? common.shadow : styles.disableBtn,
          styles.btnCont,
          btnPadding
            ? {paddingHorizontal: btnPadding, borderRadius: btnPadding}
            : null,
        ]}
        onPress={load || disable ? null : onPress}>
        <View style={[styles.container, btnStyle]}>
          {/* {!disable && !normal ? (
            <LinearGradient
              start={gradient.buttonGradient.start}
              end={gradient.buttonGradient.end}
              colors={
                !_.isEmpty(gradientBtnColor)
                  ? gradientBtnColor
                  : gradient.buttonGradient.colors
              }
              style={[
                styles.linearGradient,
                btnPadding ? {borderRadius: btnPadding} : null,
              ]}
            />
          ) : (
            <View
              style={[
                styles.linearGradient,
                btnPadding ? {borderRadius: btnPadding} : null,
              ]}
            />
          )} */}
          {!load && leftIcon ? this.renderIcon() : null}
          {load && !normal ? null : //   style={[ //   colors={gradient.buttonGradient.colors} //   end={gradient.buttonGradient.end} //   start={gradient.buttonGradient.start} // <LinearGradient
          //     styles.linearGradient,
          //     btnPadding ? {borderRadius: btnPadding} : null,
          //   ]}>
          //   <ActivityIndicator
          //     size={FORTAB ? 'small' : 'small'}
          //     color="#fff"
          //     animating
          //   />
          // </LinearGradient>
          load ? (
            <ActivityIndicator
              size={FORTAB ? 'small' : 'small'}
              color="#fff"
              animating
            />
          ) : (
            <Text numberOfLines={1} style={[styles.btnText, textStyle]}>
              {btnText}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}

CButton.propTypes = {
  btnText: PropTypes.string,
  load: PropTypes.bool,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  btnStyle: PropTypes.object || null,
  btnPadding: PropTypes.number || null,
  textStyle: PropTypes.number || null,
  leftIconSty: PropTypes.number || null,
  normal: PropTypes.bool,
  gradientBtnColor: PropTypes.arrayOf(PropTypes.any),
};

CButton.defaultProps = {
  btnText: '',
  load: false,
  onPress: null,
  disable: false,
  btnStyle: null,
  btnPadding: null,
  textStyle: null,
  leftIconSty: null,
  normal: false,
  gradientBtnColor: [],
};
