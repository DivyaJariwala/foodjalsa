import {StyleSheet} from 'react-native';
import {height} from 'react-native-dimension';
import Colors from 'Config/colors';
import * as FontFamily from 'Config/fonts';

export default StyleSheet.create({
  GeneralView: {
    marginTop: height(7),
    // fontSize: FontSize.pixel28,
    textAlign: 'center',
    color: Colors.Primary_White,
    // fontFamily: FontFamily.ExtraBold,
  },

  itemView: {width: '100%', height: height(8), justifyContent: 'center'},

  DrawerText: {
    // fontFamily: FontFamily.Regular,
    // color: Colors.Header_BackgroundColor,
    // fontSize: FontSize.pixel16,
    marginLeft: height(2),
  },
});
