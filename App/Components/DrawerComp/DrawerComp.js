import React, { useState } from 'react';
import ReactNative, {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import Colors from 'Config/colors';
import { width, height, totalSize } from 'react-native-dimension';
import Styles from './Styles';
import DrawerHeader from 'Components/DrawerHeader/DrawerHeader';
import Fontisto from 'react-native-vector-icons/Fontisto';
// import TextComponent from '../TextComponents/TextComponent';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import map from 'Images/map.png';
import support from 'Images/support.png';
import logout from 'Images/logout.png';
const { GeneralView, itemView, ItemTextView, DrawerText } = Styles;

const DrawerComp = (props) => {
  return (
    <View style={{ flex: 1, opacity: 0.95, height: height(100) }}>
      <LinearGradient
        colors={['#042C54', '#0757A8']}
        start={{ x: 0, y: 0.5 }}
        style={{ flex: 1 }}
        end={{ x: 1, y: 0.5 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <DrawerHeader
            navigateToProfile={() => props.navigation.navigate('Profile')}
          />

          <View
            style={{
              marginBottom: height(2),
              paddingLeft: width(6),
              paddingRight: width(4),
            }}>
            <View style={{ marginTop: height(7), height: height(40) }}>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Your Order')}
                style={[
                  itemView,
                  {
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  },
                ]}>
                <Image source={map} style={{ marginLeft: width(2) }}></Image>
                {/* <TextComponent
                  style={[DrawerText]}
                  text={Translotar('Drawer.YourorderText')}
                /> */}
                <Text>Test</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Finance')}
                style={[
                  itemView,
                  {
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  },
                ]}>
                <FontAwesome5
                  name="coins"
                  size={totalSize(2.5)}
                  color={Colors.Header_BackgroundColor}
                  style={{ marginLeft: width(2) }}
                />
                {/* <TextComponent
                  style={[DrawerText]}
                  text={Translotar('Drawer.financeText')}
                /> */}
                <Text>Test</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Settings')}
                style={[
                  itemView,
                  {
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  },
                ]}>
                <Ionicons
                  name="settings-outline"
                  size={totalSize(2.5)}
                  color={Colors.Header_BackgroundColor}
                  style={{ marginLeft: width(2) }}
                />
                {/* <TextComponent
                  style={[DrawerText]}
                  text={Translotar('Drawer.settingText')}
                /> */}
                <Text>Test</Text>
              </TouchableOpacity>

              {/* <View
                style={[
                  CommonStyle.RowFlexView,
                  {
                    justifyContent: 'space-between',
                    alignItems: 'flex-start',
                    marginTop: height(2),
                  },
                ]}> */}
              {/* <View> */}
              {/* <TextComponent
                    style={[DrawerText]}
                    text={Translotar('Drawer.lang')}
                  /> */}
              {/* <Text>Test</Text>
                </View>
                <TouchableOpacity onPress={() => langaugeSet()}> */}
              {/* <TextComponent
                    //  style={switchText}
                    text={
                      props.DefaultLangauge === 'en'
                        ? Translotar('Drawer.switchToEnglish')
                        : Translotar('Drawer.switchToarabic')
                    }
                  /> */}
              {/* <Text>Test</Text>
                </TouchableOpacity> */}
              {/* </View> */}
            </View>
            <View style={{ height: height(40) }}>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Support')}
                style={[
                  itemView,
                  {
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                  },
                ]}>
                <Image source={support}></Image>
                {/* <TextComponent
                  style={[DrawerText]}
                  text={Translotar('Drawer.supportText')}
                /> */}
                <Text>Test</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Finance')}
                style={[
                  itemView,
                  {
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                  },
                ]}>
                <Image source={logout}></Image>
                {/* <TextComponent
                  style={[DrawerText]}
                  text={Translotar('Drawer.logout')}
                /> */}
                <Text>Test</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    </View>
  );
};

export default DrawerComp;
