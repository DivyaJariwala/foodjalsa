import React from 'react';
import {TouchableOpacity} from 'react-native';
import {View, Text, StyleSheet, Image} from 'react-native';
import {BottomSheet} from 'react-native-btr';
import {SocialIcon} from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import whatsapp from '../../Assets/Images/WhatsApp.jpeg';
const Bottomsheet = (props) => {
  return (
    <View>
      <BottomSheet
        visible={props.visible}
        //setting the visibility state of the bottom shee
        onBackButtonPress={props.toggleBottomNavigationView}
        //Toggling the visibility state
        onBackdropPress={props.toggleBottomNavigationView}
        //Toggling the visibility state
      >
        {/*Bottom Sheet inner View*/}
        <View style={styles.bottomNavigationView}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                textAlign: 'center',
                padding: 20,
                fontSize: 20,
              }}>
              Share Using
            </Text>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <SocialIcon
                //Social Icon using react-native-elements
                type="twitter"
                //Type of Social Icon
                onPress={() => {
                  //Action to perform on press of Social Icon
                  props.twitterClick();
                  // alert('twitter');
                }}
              />

              <SocialIcon
                type="facebook"
                onPress={() => {
                  props.facebookClick();
                  // alert('facebook');
                }}
              />
              <SocialIcon
                type="instagram"
                onPress={() => {
                  props.instaClick();
                  // alert('instagram');
                }}
              />
              <TouchableOpacity
                style={{alignItems: 'center', marginTop: 5, marginLeft: 5}}
                onPress={() => {
                  props.whatsappClick();
                }}>
                <Image
                  source={whatsapp}
                  resizeMode="center"
                  style={{height: 55, width: 55}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </BottomSheet>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E0F7FA',
  },
  bottomNavigationView: {
    backgroundColor: '#fff',
    width: '100%',
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default Bottomsheet;
