import React from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import Drawer from '../Components/Divider/Divider';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from 'Config/styles';
import Color from 'Config/colors';
import {TouchableOpacity} from 'react-native';
import {height, width} from 'react-native-dimension';
function Option(props) {
  return (
    <View
      style={{
        marginVertical: 8,
        marginTop: 15,
        justifyContent: 'center',
        // alignItems: 'center',
      }}>
      <TouchableOpacity onPress={props.Options}>
        <View style={{flexDirection: 'row', width: '100%'}}>
          <View style={{width: '80%', flexDirection: 'row'}}>
            <View
              style={{
                borderRadius:
                  Math.round(
                    Dimensions.get('window').width +
                      Dimensions.get('window').height,
                  ) / 2,
                width: Dimensions.get('window').width * 0.1,
                height: Dimensions.get('window').width * 0.1,
                // width: width(10),
                // height: height(6),
                backgroundColor: props.logout ? '#FFD7D7' : '#EEF0F6',
                // borderRadius: width * height / 2,
                // borderWidth: 1,
                padding: height(1.6),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {/* <AntDesign name={props.name} size={20} color="#ADB5D3" /> */}
              <Image
                source={props.image}
                style={{height: 20, width: 20}}
                resizeMode="contain"
              />
            </View>

            <View
              style={{width: '65%', justifyContent: 'center', marginLeft: 30}}>
              <Text
                style={[
                  styles.subTxt,
                  {
                    fontSize: 15,
                    fontWeight: 'bold',
                    color: props.logout ? '#FF3C3D' : '#000000',
                  },
                ]}>
                {props.option}
              </Text>
            </View>
          </View>
          {props.rightArror ? (
            <View
              style={{
                width: '20%',
                justifyContent: 'flex-end',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <AntDesign name="right" size={18} color={Color.lightGrey} />
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
      {/* <Drawer /> */}
    </View>
  );
}

export default Option;
