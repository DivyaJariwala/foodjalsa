import {TAB_INDEX, userToken} from './actiontype';
let initial = {
  index: 0,
  token: '',
};

const reducer = (state = initial, action) => {
  switch (action.type) {
    case TAB_INDEX:
      return Object.assign({}, state, {index: action.index});
    case userToken:
      return Object.assign({}, state, {token: action.token});

    default:
      return state;
  }
};

export default reducer;
