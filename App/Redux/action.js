import {TAB_INDEX, userToken} from './actiontype';

let initialState = {
  index: 0,
  token: '',
};

const tabIndex = (index) => ({type: TAB_INDEX, index});
const usertoken = (token) => ({type: userToken, token});

export default {
  tabIndex,
  usertoken,
};
