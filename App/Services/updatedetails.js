import baseUrl from '../Config/BaseURL';
import axios from 'axios';

// -----!UPDATEPERSONALDETAILS!-----
export const updatepersonaldetails = (token, payload) => {
  return axios(baseUrl + '/personal-details', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// ------!GETPERSONLADETAIL!------
export const getpersonaldetails = (token) => {
  return axios(baseUrl + '/personal-details', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!UPDATEOUTLET!-----
export const updateoutlet = (token, payload) => {
  return axios(baseUrl + '/outlet-details', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// ------!GETOUTLET!------
export const getoutlet = (token) => {
  return axios(baseUrl + '/outlet-details', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

//-----!GETPERSONALDETAILS-------!
export const personaldetails = (token) => {
  return axios(baseUrl + '/my-account', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};
