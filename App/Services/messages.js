import baseUrl from '../Config/BaseURL';
import axios from 'axios';


// -----!GET-MESSAGES!-----
export const getmessage = token => {
    return axios(baseUrl + '/getmessages', {
        method: "GET",
        headers: {
            'content-type': 'multipart/form-data',
            'Accept': 'application/json',
            'Authorization': token
        },

    }).then(response => response.data)
        .catch(e => e.response)
}

// ------!MESSAGEREPLY!-------
export const reply = (payload, token) => {
    return axios(baseUrl + '/usersendmessage', {
        method: "post",
        headers: {
            'content-type': 'multipart/form-data',
            'Accept': 'application/json',
            'Authorization': token
        },
        data: payload

    }).then(response => response.data)
        .catch(e => e.response)
}