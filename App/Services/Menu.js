import baseUrl from 'Config/BaseURL';
import axios from 'axios';

// -----!QRCOUNT!-----
export const Qrcount = (token) => {
  return axios(baseUrl + '/get-outlet-scan', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// ------!GET MENU FILE!-------
export const menucount = (token, payload) => {
  return axios(baseUrl + '/get-menu-files', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// ------!UPLOAD MENU FILE!-------
export const upload = (token, payload) => {
  return axios(baseUrl + '/upload-menu-file', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// ----------!DELETE MENU FILE!------------
export const deletefile = (token, payload) => {
  return axios(baseUrl + '/remove-menu-files', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};
