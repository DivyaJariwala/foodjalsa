import baseUrl from '../Config/BaseURL';
import axios from 'axios';

// ------!FavouriteDeliveryList!-------
export const FavouriteBid = (payload, token) => {
    return axios(baseUrl + '/favdeliverylist', {
        method: "post",
        headers: {
            'content-type': 'multipart/form-data',
            'Accept': 'application/json',
            'Authorization': token
        },
        data: payload
    }).then(response => response.data)
        .catch(e => e.response)
}