import baseUrl from 'Config/BaseURL';
import axios from 'axios';

// -----!CATEGORY!-----
export const category = () => {
  return axios(baseUrl + '/get-outlet-types', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};
