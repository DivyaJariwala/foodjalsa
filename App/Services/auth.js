import baseUrl from 'Config/BaseURL';
import axios from 'axios';

// -----!LOGIN!-----
export const login = (payload) => {
  return axios(baseUrl + '/login', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!REGISTER!------
export const register = (payload) => {
  return axios(baseUrl + '/signup', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!FORGET-PASSWORD!-----
export const forgetpassword = (payload) => {
  return axios(baseUrl + '/forgot-password', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!CHECKOTP!------
export const checkout = (payload) => {
  return axios(baseUrl + '/verify-code', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!CHANGEPASSWORD!-----
export const changepassowrd = (payload) => {
  return axios(baseUrl + '/reset-password', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!LOGOUT!-----
export const logout = (token) => {
  return axios(baseUrl + '/logout', {
    method: 'GET',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};

// -----!RESET PASSWORD!----
export const resetpasssword = (token, payload) => {
  return axios(baseUrl + '/change-password', {
    method: 'POST',
    headers: {
      'content-type': 'multipart/form-data',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    data: payload,
  })
    .then((response) => response.data)
    .catch((e) => e.response);
};
