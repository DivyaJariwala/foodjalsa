export const authStack = {
  auth: 'AuthStack',
  login: 'LoginScreen',
  register: 'RegisterScreen',
  forgot: 'ForgotScreen',
  reset: 'ResetScreen',
  verify: 'VerifyScreen',
  signup2: 'SignUp2',
};

export const rootswitch = {
  loading: 'LoadingScreen',
  auth: 'AuthStack',
  main: 'MainStack',
};

export const mainStack = {
  home: 'HomeScreen',
  cart: 'CartScreen',
  sidebar: 'SideBar',
  setting: 'Setting',
  orders: 'Orders',
  bottomtab: 'BottomTab',
  category: 'Category',
  menu: 'Menu',
  preview: 'Preview',
};

export const settingStack = {
  personaldetails: 'EditPersonalDetails',
  outletdetails: 'EditOutletDetails',
  changepassword: 'ChangePassword',
  logout: 'Logout',
};
