import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');
const FORTAB = width <= 2736 && width >= 600;
const TABLANDSCAPE = width <= 2736 && width >= 600;
const TABPORTRAIT = width >= 600 && width <= 600;

const breakPoints = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

const XS = width < breakPoints.sm && width >= breakPoints.xs;
const SM = width < breakPoints.md && width >= breakPoints.sm;
const MD = width < breakPoints.lg && width >= breakPoints.md;
const LG = width < breakPoints.xl && width >= breakPoints.lg;
const XL = width >= breakPoints.xl;

export {FORTAB, TABLANDSCAPE, TABPORTRAIT, XS, SM, MD, LG, XL};
