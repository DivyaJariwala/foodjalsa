import {Dimensions, Platform, PixelRatio} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {FORTAB} from './MQ';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export function normalize(size, round = 'yes') {
  return RFValue(size);
}

const fonts = {
  overPass: {
    regular: 'Overpass-Regular',
    semiBold: 'Overpass-SemiBold',
    bold: 'Overpass-Bold',
    exbold: 'Overpass-ExtraBold',
    light: 'Overpass-Light',
    exlight: 'Overpass-ExtraLight',
    black: 'Overpass-Black',
  },
  roboto: {
    regular: 'Roboto-Regular',
    semiBold: 'Roboto-Medium',
    bold: 'Roboto-Bold',
    light: 'Roboto-Light',
    exlight: 'Roboto-Thin',
    black: 'Roboto-Black',
  },
  UniSans: {
    heavy: 'UniSansHeavyCAPS',
    thin: 'UniSansThinCAPS',
  },
  Poppins: {
    regular: 'Poppins-Regular',
  },
};

export default fonts;
