import {StyleSheet} from 'react-native';
import {height, width} from 'react-native-dimension';
import Colors from 'Config/colors';


export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    // marginHorizontal: height(2),
    paddingVertical:height(2),
    paddingHorizontal: height(2),
    flex:1
  },
});
