/* eslint-disable global-require */

export const formattedWords = {
  'image/jpeg': 'JPEG',
  'image/gif': 'GIF',
  'image/png': 'PNG',
  'application/pdf': 'PDF',
  'text/plain': 'PLAIN',
  'text/csv': 'CSV',
  'audio/mpeg': 'MPEG',
  'audio/ogg': 'OGG',
};

export const TopDrawerList = [
  {
    id: 1,
    value: 'Home',
    iconName: 'home',
    screen: 'Home',
  },
  {
    id: 2,
    value: 'Messages',
    iconName: 'message1',
    screen: 'Messages',
  },
  {
    id: 3,
    value: 'Profile',
    iconName: 'profile',
    screen: 'Profile',
  },
];
