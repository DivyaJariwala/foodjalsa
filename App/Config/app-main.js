import {Navigation} from 'react-native-navigation';
// import {Provider} from 'react-redux';
import _ from 'lodash';
import {Platform, Dimensions, Text} from 'react-native';
// import {persistStore} from 'redux-persist';
// import RNRestart from 'react-native-restart';
// import {
//   setJSExceptionHandler,
//   setNativeExceptionHandler,
// } from 'react-native-exception-handler';
import {registerScreens} from '../index';
// import {store} from '../redux/store/configureStore';
import settings from './settings';
import setHome from './route/Home';

// Remove font scale
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

function setDefaultOpts() {
  const defOpts = {
    topBar: {
      visible: false,
      drawBehind: true,
    },
    layout: {
      orientation: ['portrait'],
      backgroundColor: '#FFF',
    },
    statusBar: {
      visible: true,
      style: 'dark',
      backgroundColor: '#FFF',
      drawBehind: false,
    },
    bottomTabs: {
      animate: true,
      drawBehind: false,
      backgroundColor: '#f5f8fe',
      hideShadow: true,
      titleDisplayMode: 'alwaysShow',
    },
    animations: {
      setRoot: {
        enabled: 'true', // Optional, used to enable/disable the animation
        waitForRender: 'true',
        alpha: {
          from: 0,
          to: 1,
          duration: 600,
          startDelay: 0,
          interpolation: 'accelerate',
        },
      },
    },
  };

  if (Platform.OS !== 'ios') {
    defOpts.animations.push = {
      enabled: 'true',
      waitForRender: 'true',
      content: {
        alpha: {
          from: 0,
          to: 1,
          duration: 300,
          startDelay: 0,
          interpolation: 'accelerate',
        },
        x: {
          from: 300,
          to: 0,
          duration: 300,
          startDelay: 0,
          interpolation: 'accelerate',
        },
      },
    };

    defOpts.animations.pop = {
      enabled: 'true',
      waitForRender: 'true',
      content: {
        alpha: {
          from: 1,
          to: 0,
          duration: 300,
          startDelay: 0,
          interpolation: 'accelerate',
        },
        x: {
          from: 0,
          to: -300,
          duration: 500,
          startDelay: 0,
          interpolation: 'accelerate',
        },
      },
    };
  }

  Navigation.setDefaultOptions(defOpts);
}

export default function startMain(token, userData, welcome) {
  console.log(this.props);
  console.log('======== start app with params ========');
  console.log(`token : ${token}`);
  console.log('======== user data ========');
  console.log('userData--=');
  console.log(userData);
  console.log(welcome);
  console.log(token);
  // iconsLoaded.then(() => {

  /* Default Start Page should be Welcome as per client confirmation */
  let startPage = 'Welcome';
  if (welcome) {
    startPage = 'Welcome';
  } else if (token !== '') {
    /* Change start page in case of user is logged in */
    /* If user is in - Show page according to his profile */
    if (token !== '' && _.isObject(userData)) {
      startPage = 'Home';
    }
  }
  // startPage = 'Welcome';
  /* Quick hack to show Splash for 3 seconds of time */
  setTimeout(
    () => {
      /* Set Default Navigation options - depends on Start screen */
      setDefaultOpts();
      if (token !== '' && _.isObject(userData)) {
        setHome();
      } else {
        Navigation.setRoot({
          root: {
            sideMenu: {
              id: 'mainSideMenu',
              center: {
                stack: {
                  id: 'appStack',
                  children: [
                    {
                      component: {
                        name: startPage,
                      },
                    },
                  ],
                },
              },
              options: {
                sideMenu: {
                  openGestureMode: 'bezel',
                  right: {
                    width: Dimensions.get('window').width - 100,
                  },
                },
              },
            },
          },
        });
      }
    },
    __DEV__ ? 0 : settings.splashDelay,
  );
}

export const startAll = () => {
  Navigation.events().registerAppLaunchedListener(() => {
    console.log('Start ALL Registered Listern ==================== >>>>>>>');
    
    persistStore(store, null, () => {
      console.log(store.getState());
      const {
        auth: {token, userData, welcome},
      } = store.getState();

      startMain(token, userData, welcome);
      // console.log(startMain(token, userData, welcome));
      registerScreens(store, Provider);
    });
  });
};

// const errorHandler = (e, isFatal) => {
//   Alert.alert(
//     'Unexpected error occurred',
//     `
//     Error: ${(isFatal) ? 'Fatal:' : ''} ${e.name} ${e.message}

//     We will need to restart the app.
//     `,
//     [{
//       text: 'Restart',
//       onPress: () => {
//         RNRestart.Restart();
//       },
//     }],
//   );
// };

// setJSExceptionHandler(errorHandler);
// setNativeExceptionHandler((errorString) => {
//   Alert.alert(
//     'Native Unexpected error occurred',
//     errorString,
//     [{
//       text: 'Restart',
//       onPress: () => {
//         RNRestart.Restart();
//       },
//     }],
//   );
// });
