import {PixelRatio, Platform} from 'react-native';
import {FORTAB} from '../config/MQ';
import MaterialIcons from '../libs/react-native-vector-icons/MaterialIcons';
import FontAwesome5Icons from '../libs/react-native-vector-icons/FontAwesome5';
import Entypo from '../libs/react-native-vector-icons/Entypo';
import Octicons from '../libs/react-native-vector-icons/Octicons';
import Anticons from '../libs/react-native-vector-icons/AntDesign';

const iconSize = FORTAB ? 30 : 20;
const navIconSize =
  __DEV__ === false && Platform.OS === 'android'
    ? PixelRatio.getPixelSizeForLayoutSize(8)
    : iconSize;
const replaceSuffixPattern = /--(active|big|small|very-big)/g;

const icons = {
  'add-circle': [30, '#000000'],
  'keyboard-arrow-left': [navIconSize, '#FFF'],
};
const oicons = {
  home: [navIconSize, '#000'],
};
const ficons = {
  bars: [navIconSize, '#000'],
  search: [navIconSize, '#FFFFFF'],
  newspaper: [navIconSize, '#FFFFFF'],
  comments: [navIconSize, '#FFFFFF'],
  gem: [navIconSize, '#FFFFFF'],
  'user-circle': [navIconSize, '#FFFFFF'],
  'chevron-left': [navIconSize, '#FFFFFF'],
  'arrow-left': [navIconSize, '#FFFFFF'],
  'less-than': [navIconSize, '#FFF'],
  'map-marked-alt': [navIconSize, '#FFF'],
  'sliders-h': [navIconSize, '#FFFFFF'],
  'pencil-alt': [navIconSize, '#FFFFFF'],
};

const eIcon = {
  'dots-three-horizontal': [navIconSize, '#FFFFFF'],
  message: [navIconSize, '#000'],
};
const anticons = {
  profile: [navIconSize, '#000'],
};
const iconsArray = [
  [icons, MaterialIcons],
  [ficons, FontAwesome5Icons],
  [eIcon, Entypo],
  [oicons, Octicons],
  [anticons, Anticons],
];

const iconsMap = {};
const iconsLoaded = new Promise(resolve => {
  const allFonts = [iconsArray].map(iconArrayMain => {
    return Promise.all(
      iconArrayMain.map(iconArray => {
        return Promise.all(
          Object.keys(iconArray[0]).map(iconName =>
            // IconName--suffix--other-suffix is just the mapping name in iconsMap
            iconArray[1].getImageSource(
              iconName.replace(replaceSuffixPattern, ''),
              iconArray[0][iconName][0],
              iconArray[0][iconName][1],
            ),
          ),
        ).then(sources => {
          return Object.keys(iconArray[0]).forEach(
            (iconName, idx) => (iconsMap[iconName] = sources[idx]),
          );
          // resolve(true);
        });
      }),
    ).then(() => {
      resolve(true);
    });
  });

  return Promise.all(allFonts);
});

export {iconsMap, iconsLoaded};
