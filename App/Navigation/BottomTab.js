import React from 'react';
import BottomTabComp from '../Components/BottomTabComp/Bottomtabcomp';
import Home from '../Screens/Home/Container';
import Setting from '../Screens/Settings/Container';
import Cart from '../Screens/Cart/Container';
import {mainStack} from '../Config/Navigator';
import cartstack from '../Navigation/Qrcode';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

function BottomTab() {
  return (
    <Tab.Navigator
      initialRouteName={mainStack.home}
      // tabBarPosition="bottom"
      tabBar={(props) => <BottomTabComp {...props} />}>
      <Tab.Screen name={mainStack.home} component={Home} />
      <Tab.Screen name={mainStack.setting} component={Setting} />
      <Tab.Screen name={mainStack.cart} component={cartstack} />
    </Tab.Navigator>
  );
}

export default BottomTab;
