import React from 'react';
import {authStack, settingStack} from '../Config/Navigator';
import {createStackNavigator} from '@react-navigation/stack';
import PersonalDetails from '../Screens/Settings/EditDetails';
import OutletDetails from '../Screens/Settings/EditOutlet';
import ChangePassword from '../Screens/Settings/ChangePassword';
import Login from '../Screens/Auth/Login';
const Stack = createStackNavigator();

function Setting() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen
        name={settingStack.personaldetails}
        component={PersonalDetails}
      />
      <Stack.Screen
        name={settingStack.outletdetails}
        component={OutletDetails}
      />
      <Stack.Screen
        name={settingStack.changepassword}
        component={ChangePassword}
      />
      <Stack.Screen name={authStack.login} component={Login} />
    </Stack.Navigator>
  );
}

export default Setting;
