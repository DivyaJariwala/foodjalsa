import React from 'react';
import {authStack, mainStack} from '../Config/Navigator';
import {createStackNavigator} from '@react-navigation/stack';
import cart from '../Screens/Cart/Container';
const Stack = createStackNavigator();

function Qrcode() {
  return (
    <Stack.Navigator headerMode="none" initialRouteName={mainStack.cart}>
      <Stack.Screen name={mainStack.cart} component={cart} />
     
    </Stack.Navigator>
  );
}

export default Qrcode;
