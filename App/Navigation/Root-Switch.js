import React, {useEffect, useState} from 'react';
import {
  rootswitch,
  mainStack,
  settingStack,
  authStack,
} from '../Config/Navigator';
import Auth from '../Navigation/auth-stack';
import main from '../Navigation/BottomTab';
import Bottomtab from '../Navigation/BottomTab';
import {createStackNavigator} from '@react-navigation/stack';
import Loading from '../Screens/Loading';
import Setting from '../Navigation/setting-stack';
import PersonalDetails from '../Screens/Settings/EditDetails';
import OutletDetails from '../Screens/Settings/EditOutlet';
import ChangePassword from '../Screens/Settings/ChangePassword';
import menu from '../Screens/Cart/Menu';
import login from '../Screens/Auth/Login';
import Preview from '../Screens/Cart/Imageview';
import Mainstack from './Mainstack';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

const RootSwitch = () => {
  var userToken;
  var route;
  useEffect(() => {
    _bootstrapAsync();
  }, []);
  const _bootstrapAsync = async () => {
    userToken = await AsyncStorage.getItem('@Token');
    route = userToken ? mainStack.home : rootswitch.auth;

    console.log(userToken);
    console.log(route);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
  };
  return (
    <Stack.Navigator headerMode="none" name={rootswitch.loading}>
      <Stack.Screen name={rootswitch.loading} component={Loading} />

      <Stack.Screen name={mainStack.home} component={Bottomtab} />

      <Stack.Screen name={rootswitch.auth} component={Auth} />

      <Stack.Screen
        name={settingStack.personaldetails}
        component={PersonalDetails}
      />
      <Stack.Screen
        name={settingStack.outletdetails}
        component={OutletDetails}
      />
      <Stack.Screen
        name={settingStack.changepassword}
        component={ChangePassword}
      />
      <Stack.Screen name={mainStack.setting} component={Setting} />
      <Stack.Screen name={mainStack.menu} component={menu} />
      <Stack.Screen name={authStack.login} component={login} />
      <Stack.Screen name={mainStack.preview} component={Preview} />
    </Stack.Navigator>
  );
};

export default RootSwitch;
