import React, {useEffect, useState} from 'react';
import {
  rootswitch,
  mainStack,
  settingStack,
  authStack,
} from '../Config/Navigator';
import Auth from '../Navigation/auth-stack';
import main from '../Navigation/BottomTab';
import Bottomtab from '../Navigation/BottomTab';
import {createStackNavigator} from '@react-navigation/stack';
import Loading from '../Screens/Loading';
import Setting from '../Navigation/setting-stack';
import PersonalDetails from '../Screens/Settings/EditDetails';
import OutletDetails from '../Screens/Settings/EditOutlet';
import ChangePassword from '../Screens/Settings/ChangePassword';
import menu from '../Screens/Cart/Menu';
import login from '../Screens/Auth/Login';
const Stack = createStackNavigator();

const Mainstack = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name={mainStack.home} component={Bottomtab} />
      <Stack.Screen
        name={settingStack.personaldetails}
        component={PersonalDetails}
      />
      <Stack.Screen
        name={settingStack.outletdetails}
        component={OutletDetails}
      />
      <Stack.Screen
        name={settingStack.changepassword}
        component={ChangePassword}
      />
      <Stack.Screen name={mainStack.setting} component={Setting} />
      <Stack.Screen name={mainStack.menu} component={menu} />
      <Stack.Screen name={authStack.login} component={login} />
    </Stack.Navigator>
  );
};

export default Mainstack;
