import React from 'react';
import {authStack} from '../Config/Navigator';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../Screens/Auth/Login';
import Register from '../Screens/Auth/Register';
import Forgetpass from '../Screens/Auth/ForgetPassword';
import Verifycode from '../Screens/Auth/VerifyCode';
import Resetpass from '../Screens/Auth/ResetPassword';
import Signup2 from '../Screens/Auth/Signup2';
const Stack = createStackNavigator();

function Auth() {
  return (
    <Stack.Navigator headerMode="none" >
      <Stack.Screen name={authStack.login} component={Login} />
      <Stack.Screen name={authStack.register} component={Register} />
      <Stack.Screen name={authStack.signup2} component={Signup2} />
      <Stack.Screen name={authStack.forgot} component={Forgetpass} />
      <Stack.Screen name={authStack.verify} component={Verifycode} />
      <Stack.Screen name={authStack.reset} component={Resetpass} />
    </Stack.Navigator>
  );
}

export default Auth;
