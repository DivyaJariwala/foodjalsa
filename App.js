import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import RootSwitch from './App/Navigation/Root-Switch';
import {Provider} from 'react-redux';
import {store} from './App/Redux/store';
import SplashScreen from 'react-native-splash-screen';
import AsyncStorage from '@react-native-community/async-storage';
import {rootswitch, mainStack} from './App/Config/Navigator';
// import {useNavigation} from '@react-navigation/native';
import Mainstack from './App/Navigation/Mainstack';
import Auth from './App/Navigation/auth-stack';
import {userToken} from './App/Redux/actiontype';

const App = (props) => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootSwitch />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
